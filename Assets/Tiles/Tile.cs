using System;
using System.Collections.Generic;
using Conquest.Assets.Graphics;
using Conquest.Assets.Entity;

namespace Conquest.Assets.Tiles
{
	public class Tile
	{
		private Sprite sprite;
		private int id;
		private static int lastID = 0;
		
		public Tile (Sprite sprite)
		{
			this.sprite = sprite;
			id = lastID;
			lastID++;
			tiles.Add(this);
		}
		
		public int ID
		{
			get {return id;}	
		}
		
		public Sprite GetSprite
		{
			get {return sprite;}	
		}
		
		
		public static List<Tile> tiles = new List<Tile>();
		
		public static readonly Tile Grass = new Tile(Sprite.Grass);
		public static readonly Tile Dirt = new Tile(Sprite.Dirt);
		public static readonly Tile Sand = new Tile(Sprite.Sand);
		public static readonly Tile Tree = new Tile(Sprite.Tree);
		public static readonly Tile Brush = new Tile(Sprite.Brush);
		public static readonly Tile Rock = new Tile(Sprite.Rock);
		public static readonly Tile Water = new Tile(Sprite.Water);
		public static readonly Tile Ice = new Tile(Sprite.Ice);
		
		private static readonly int[,] defensiveScore;
		private static readonly int [,] movementScore;
		
		static Tile()
		{
			defensiveScore = new int[,]
				/*Soldier	Bandit	BountyHunter */
			{
/*grass:*/		{1,			0,		1},
/*dirt:*/		{1,			0,		1},
/*sand:*/		{1,			0,		2},
/*tree:*/		{4,			5,		4},
/*brush:*/		{3,			4,		4},
/*rock:*/		{5,			5,		6},
/*water:*/		{0,			0,		2},
/*ice:*/		{1,			1,		1}
			};
			
			
			movementScore = new int[,]
				/*Soldier	Bandit 	BountyHunter*/
			{
/*grass:*/		{2,			2,		2},
/*dirt:*/		{1,			2,		2},
/*sand:*/		{4,			4,		3},
/*tree:*/		{4,			2,		3},
/*brush:*/		{5,			3,		4},
/*rock:*/		{6,			5,		4},
/*water:*/		{0,			-1,		0},
/*ice:*/		{8,			7,		6}
			};
		}
		
		public static int GetDefensiveScore(int TileID, int defenderTypeID)
		{
			return defensiveScore[TileID, defenderTypeID];
		}
		
		public static int GetMovementScore(int TileID, int moverTypeID)
		{
			return movementScore[TileID, moverTypeID];
		}

	}
}

