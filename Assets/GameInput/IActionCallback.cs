using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.GameInput
{
	public interface IActionCallback
	{
		void SetMoveZone(IEnergeticMovable movable);
		bool MoveAndAnimate(IEnergeticMovable movable, int destinationX, int destinationY);
		void Attack();
		void CloseInteractMenu();
	}
}

