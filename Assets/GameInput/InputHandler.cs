using System;
using UnityEngine;
using Conquest.Assets.Entity;
using System.Collections.Generic;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Graphics;
using Conquest.Assets.Graphics.UtilityGUI;
using Conquest.Assets.Teams;

namespace Conquest.Assets.GameInput
{
	public class InputHandler: IActionCallback
	{
		public IEntity SelectedEntity {get; private set;} //maybe does not belong here??
		public EntityMovementAnimator movementAnimator; //does not belong here, requires refactor
		public MoveNode MovementZone {get; private set;} //maybe refactor
		public bool SelectorEnabled {get; set;}
		private World world;
		private const int VIEWPORT_SCROLL_SENSITIVITY = 250*4; //in pixels per second
	
		
	//	public bool InteractionMode { get; private set; }
		
		public InputHandler (World world)
		{
			SelectorEnabled = true;
			movementAnimator = new EntityMovementAnimator(this);
			this.world = world;
			
		}
		
		public bool HasSelectedEntity()
		{
			return SelectedEntity != null;	
		}
		
		public void Deselect()
		{
			SelectedEntity = null;
			FreeMovementZone();
			SelectorEnabled = true;
		//	InteractionMode = false;
			
			if (movementAnimator.IsAnimating())
			{
				movementAnimator.EndAnimation();	
			}
			
			bool IsReportingSpecial = Game.guiManager.IsState(GUIManager.GUIState.ReportSpecialMessage);
			
			Game.guiManager.UnsetAllStates();
			Game.guiManager.CloseSpecialReport();
			if (Game.guiManager.specialReports.Count > 0)
			{
				Game.guiManager.SetState(GUIManager.GUIState.ReportSpecialMessage);	
			}
		}
		
		public void EndMovement()
		{
			FreeMovementZone();
			SelectorEnabled = true;
			SelectEntity(SelectedEntity);
		}
		
		public IEntity Select(int xWorld, int yWorld)
		{
			IEntity clickedEntity = world.entityManager.GetEntityAt(xWorld, yWorld);
			
			if (clickedEntity != null)
			{
				if (clickedEntity != SelectedEntity)
				{
					Deselect();	
				}
				//if (clickedEntity.IsOwnedByMainPlayer())
				//{
					SelectEntity(clickedEntity);
				//}
			}
			else
			{
				return null;
			}
			
			return clickedEntity;
		}
		
		public void SetMoveZone(IEnergeticMovable movable)
		{
			SetMovementZone(movable.FindMovableZone(world));
		}
		
		public void SelectEntity(IEntity entityToSelect)
		{
			SelectedEntity = entityToSelect;
			
			if (SelectedEntity == null) return;
			
			if (SelectedEntity.GetOwner() == Team.mainPlayer)
			{
				SelectedEntity.DoSelectAction(world, this);
			}
			else
			{
				FreeMovementZone();
			}
			Game.guiManager.SetState(GUIManager.GUIState.ShowEntityStatus);
		}
		
		//public void 
		
		public void FreeMovementZone()
		{
			MovementZone = null;
		}
		
		public void SetMovementZone(MoveNode movementZone)
		{
			MovementZone = movementZone;	
		}
		
		public bool IsMovementZoneSet()
		{
			return MovementZone != null;	
		}
		
		
		
		//get mosue positions based on world position in pixels
		public float GetWorldPixelMouseX()
		{
			return Input.mousePosition.x - Screen.width/2 + world.ScrollX;
		}
		
		public float GetWorldPixelMouseY()
		{
			return -Input.mousePosition.y + Screen.height/2 + world.ScrollY;
		}
		
		public int GetWorldPositionMouseX()
		{
			return (int)(GetWorldPixelMouseX()) >> SpriteSheet.ICON_SHIFT_WIDTH;
		}
		
		public int GetWorldPositionMouseY()
		{
			return (int)(GetWorldPixelMouseY()) >> SpriteSheet.ICON_SHIFT_HEIGHT;
		}
		
		public void HandleInputs()
		{
			
			if (world.turnHandler.ActivePlayer == Team.mainPlayer)
			{

				if (Input.GetMouseButtonDown(0)) //left click
				{
					HandleLeftClick();
				}
				
				if (Input.GetMouseButtonDown(1)) //right click
				{
					HandleRightClick();
				}
			}

			HandleEdgeScrolling();
			HandleKeyInputs();
		}
		
		public void HandleEdgeScrolling()
		{
			int limit = 3;
			
			float scrollDistanceThisFrame = VIEWPORT_SCROLL_SENSITIVITY * UnityEngine.Time.deltaTime;
			
			if (Input.mousePosition.x < limit)
			{
				world.ScrollRight(-scrollDistanceThisFrame);
			}
			else if (Input.mousePosition.x >= Screen.width - limit)
			{
				world.ScrollRight(scrollDistanceThisFrame);
			}
			
			if (Input.mousePosition.y < limit)
			{
				world.ScrollDown(scrollDistanceThisFrame);
			}
			else if (Input.mousePosition.y >= Screen.height - limit)
			{
				world.ScrollDown(-scrollDistanceThisFrame);
			}
			
			/*if (!Input.GetMouseButton(1)) return;
			
			
			float scrollDistanceThisFrame = 10*UnityEngine.Time.deltaTime;
			
			Vector2 scrollVec = new Vector2(Input.mousePosition.x - Screen.width/2, -Input.mousePosition.y + Screen.height/2);
			if (scrollVec.x == 0 && scrollVec.y == 0) return;
			

			scrollVec *= scrollDistanceThisFrame;
			
			world.ScrollDown(scrollVec.y);
			world.ScrollRight(scrollVec.x);*/
			
		}
		
		public bool IsOnGUI()
		{
			return Game.guiManager.IsOnGUI(Input.mousePosition.x, Screen.height -Input.mousePosition.y);
		}
		
		public void HandleLeftClick()
		{
			if (!SelectorEnabled || IsOnGUI()) return;
			
			int xWorldClick = GetWorldPositionMouseX();
			int yWorldClick = GetWorldPositionMouseY();
				
			IEntity entity = Select(xWorldClick, yWorldClick);
			
			UnityEngine.Debug.Log("Click: " + xWorldClick + " " + yWorldClick);
			
			if (entity == null)
			{
				Deselect();	
			}
		}
		
		public bool MoveAndAnimate(IEnergeticMovable movable, int destinationX, int destinationY)
		{
			if (IsMovementZoneSet())
			{	
				MoveNode destinationNode = MovementZone.FindCoordsInChildren(destinationX, destinationY);
				if (destinationNode != null)
				{
					LinkedList<MoveNode> path = MoveNode.ExtractPathFromMovementZone(destinationNode);
					movementAnimator.StartMove(movable, path, null);
					FreeMovementZone();
					SelectorEnabled = false;
					GameGUI.guiManager.UnsetState(GUIManager.GUIState.PromptEntityAction);
					return true;
				}
			}
			
			return false;
		}
		
		public void Attack()
		{
			FreeMovementZone();
			if (SelectedEntity != null && !SelectedEntity.IsDestroyed())
			{
				SelectEntity(SelectedEntity);
			}
		}
		
		public void CloseInteractMenu()
		{
			GameGUI.guiManager.UnsetState(GUIManager.GUIState.PromptEntityAction);	
		}
		
		public void HandleRightClick()
		{
			if (!SelectorEnabled || IsOnGUI()) return;
			
			int xWorldClick = GetWorldPositionMouseX();
			int yWorldClick = GetWorldPositionMouseY();
			
			IEntity e = world.entityManager.GetEntityAt(xWorldClick, yWorldClick);
			
			if (SelectedEntity != null && SelectedEntity.GetOwner() == Team.mainPlayer)
			{
				
				ICommunicator com = e as ICommunicator;
				
				if (e == null)
				{
					SelectedEntity.DoInputCommand(world, this, xWorldClick, yWorldClick);	
				}
				else
				{
					List<ActionButton> buttons;
					if (SelectedEntity.CanInteract(world, this, e, out buttons))
					{
						GameGUI.guiManager.RequestActionPrompt(Input.mousePosition.x, Screen.height -Input.mousePosition.y, buttons);
					}
				}
			}
		}
		
		public void EndTurnCommand()
		{		
			Deselect();
			world.turnHandler.CycleTurn(world);	
		}
		
		private void CycleEntities()
		{
			List<IEntity> entities = world.entityManager.GetEntitiesOfTeam(Team.mainPlayer);
			if (entities.Count == 0) return;
			
			int index = 0;
			if (SelectedEntity != null && SelectedEntity.GetOwner() == Team.mainPlayer)
			{
				index = entities.IndexOf(SelectedEntity);	
				index++;
				
				
				if (index == entities.Count)
				{
					index = 0;	
				}
				
			}
				
			SelectEntity(entities[index]);
			world.FocusViewportOnTile(SelectedEntity.GetWorldX(), SelectedEntity.GetWorldY());
		}
		
		public void HandleKeyInputs()
		{
			//UnityEngine.Time.deltaTime * 60 ~ Attempt to smooth scrolling for inconsistent frame rates
			//Note, refactor by adding scrolling methods to world class (or something similar) - implement maximum scroll distance and mouse edge scrolling
			float scrollDistanceThisFrame = VIEWPORT_SCROLL_SENSITIVITY * UnityEngine.Time.deltaTime;
			if (Input.GetKey(KeyCode.D)) world.ScrollRight(scrollDistanceThisFrame);
			if (Input.GetKey(KeyCode.A)) world.ScrollRight(-scrollDistanceThisFrame);
			if (Input.GetKey(KeyCode.W)) world.ScrollDown(-scrollDistanceThisFrame);
			if (Input.GetKey(KeyCode.S)) world.ScrollDown(scrollDistanceThisFrame);
			if (Input.GetKey(KeyCode.Escape)) Deselect();
			if (Input.GetKeyDown(KeyCode.Q))
			{
				if (!Game.guiManager.IsState(GUIManager.GUIState.ReportSpecialMessage))
				{
					Game.guiManager.RequestQuestReport();
				}
				else
				{
					Game.guiManager.CloseSpecialReport();
				}
			}
			
			if (Input.GetKeyDown(KeyCode.E))
			{
			//	InteractionMode = !InteractionMode;	
			}
			
			if (Input.GetKeyDown(KeyCode.F))
			{
				CycleEntities();	
			}
		}
	}
}

