using UnityEngine;

using System.Collections;

public class CameraScale: MonoBehaviour 
{
	public static float scale = 1;
    public const float pixelResolution = 1f;

    void Awake () 
    {
		//Screen.fullScreen = true;
		camera.orthographic = true;

        camera.orthographicSize = Screen.height / (float)(2 * pixelResolution);
        camera.ResetAspect();
		Screen.showCursor = true;
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 1;
    }

}