using System;
using UnityEngine;
using Conquest.Assets.GameInput;
using Conquest.Assets.Entity.Movement;

namespace Conquest.Assets.Graphics
{
	public class GameGraphics
	{
		public Vector3[] vertices;
	    public int[] triangles;
	    public Vector2[] uvs;
		protected Mesh mesh;
		protected int dataFactor;
		protected int c;
		protected int atlasDim;
		protected float textureBlock;
		
		public GameGraphics (Mesh mesh, int dataFactor)
		{
			this.mesh = mesh;
			this.dataFactor = dataFactor;
			mesh.MarkDynamic();
			
			vertices = new Vector3[4 * dataFactor];
	        triangles = new int[6 * dataFactor];
	        uvs = new Vector2[4 * dataFactor];
			
			atlasDim = Sprite.atlasDimensions;
			textureBlock = 1 / (float)atlasDim;
		}
		
		public void Initialise(int dataFactor)
		{
			vertices = new Vector3[4 * dataFactor];
	        triangles = new int[6 * dataFactor];
	        uvs = new Vector2[4 * dataFactor];
		}
		
		private void RenderMoveTree (World world, MoveNode node)
		{
			RenderTileToWorldCoords(world, Sprite.MovementIndicator.index, node.X, node.Y, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
		 
			for (int i = 0; i < node.BranchCount; i++)
			{		
				RenderMoveTree(world, node.GetChildNode(i));	
			}
		}
		
		public void BuildMesh(World world, InputHandler gui)
		{
			mesh.Clear();
			c = 0;
			
			world.Render(this);
			RenderMoveTree(world, gui);
			
			System.Array.Resize(ref vertices, c * 4);
	        System.Array.Resize(ref triangles, c * 6);
	        System.Array.Resize(ref uvs, c * 4);
			mesh.vertices = vertices;
        	mesh.triangles = triangles;
        	mesh.uv = uvs;
		}
		
		private void RenderMoveTree(World world, InputHandler gui)
		{
			MoveNode node = gui.MovementZone;
			gui.movementAnimator.Render(this, world);
			if (node != null)
			{
				for (int i = 0; i < node.BranchCount; i++)
				{		
					RenderMoveTree(world, node.GetChildNode(i));	
				}
			}
		}
		
		public void RenderTileToWorldCoords(World world, int index, int xWorld, int yWorld, int xSize, int ySize)
		{
			int mappedX = (xWorld << SpriteSheet.ICON_SHIFT_WIDTH) - world.ScrollXi;
			int mappedY = (yWorld << SpriteSheet.ICON_SHIFT_HEIGHT) - world.ScrollYi;
			
			RenderAtlasTile(index, mappedX, mappedY, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
		}
		
		public void RenderTileToWorldCoords(World world, int index, float xWorld, float yWorld, int xSize, int ySize)
		{
			float mappedX = (xWorld * SpriteSheet.ICON_WIDTH - world.ScrollX);
			float mappedY = (yWorld * SpriteSheet.ICON_HEIGHT - world.ScrollY);
			
			RenderAtlasTile(index, mappedX, mappedY, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
		}
		
		public void RenderAtlasTile(int index, float x, float y, float z, int xSize, int ySize)
	    {
	        (vertices[c * 4]).z = z;
	        (vertices[c * 4 + 1]).z = z;
	        (vertices[c * 4 + 2]).z = z;
	        (vertices[c * 4 + 3]).z = z;
	        RenderAtlasTile(index, x, y, xSize, ySize);
	    }
	
		//render a particular tile
		public void RenderAtlasTile(int index, float x, float y, int xSize, int ySize)
	    {
			
			int uvx = getIndexCol(index,atlasDim);
			int uvy = getIndexRow(index,atlasDim);
	
	
			uvy++;	//some offset thing (for texture atlas)
			
			/*
			 * dunno what offset is for, even at time of coding. Just put it here
			 * coz it fixed some graphics glitch where the background or other 
			 * funny pixels would seep through between quads in the mesh.
			 * Dodgy I know, but you try to fix it.
	         * 
	         * The offset basically reduces the lengths of the quads so that they don't
	         * overlap. If reduced too much, tears will appear. Dunno why they overlap in the first place
			 */
	
	
	       // xSize = 32;
	        //ySize = 32;
	
			
	        (vertices[c * 4]).x = x + 0.5f / (float)CameraScale.pixelResolution;
	        (vertices[c * 4]).y = -y - 0.5f / (float)CameraScale.pixelResolution;
	
	        (vertices[c * 4 + 1]).x = xSize + x + 0.5f / (float)CameraScale.pixelResolution;
	        (vertices[c * 4 + 1]).y = -y - 0.5f / (float)CameraScale.pixelResolution;
	
	        (vertices[c * 4 + 2]).x = x + 0.5f / (float)CameraScale.pixelResolution;
	        (vertices[c * 4 + 2]).y = -y - ySize - 0.5f / (float)CameraScale.pixelResolution;
	
	        (vertices[c * 4 + 3]).x = xSize + x + 0.5f / (float)CameraScale.pixelResolution;
	        (vertices[c * 4 + 3]).y = -y - ySize - 0.5f / (float)CameraScale.pixelResolution;
	
			
			triangles[c*6] 	 =	c*4;
			triangles[c*6+1] =  c*4+1;
			triangles[c*6+2] =  c*4+2;
			triangles[c*6+3] =  c*4+3;
			triangles[c*6+4] =  c*4+2;
			triangles[c*6+5] =  c*4+1;
	
	
	        //investigate further and then possibly remove texture pixel completely
	        (uvs[c * 4]).x = (float)(textureBlock * uvx);
	        (uvs[c * 4]).y = (float)(textureBlock * uvy);
	        (uvs[c * 4 + 1]).x = (float)(textureBlock * (uvx + 1));
	        (uvs[c * 4 + 1]).y = (float)(textureBlock * (uvy));
	        (uvs[c * 4 + 2]).x = (float)(textureBlock * uvx);
	        (uvs[c * 4 + 2]).y = (float)(textureBlock * (uvy - 1));
	        (uvs[c * 4 + 3]).x = (float)(textureBlock * (uvx + 1));
	        (uvs[c * 4 + 3]).y = (float)(textureBlock * (uvy - 1));
			
			c++;
		}
		
	
		public int getIndexCol(int index, int cols)
	    {
	        return (index % cols);
	    }
	
	    public int getIndexRow(int index, int cols)
	    {
	        return (index / cols);
	    }
	}
}

