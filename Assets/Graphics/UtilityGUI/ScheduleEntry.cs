using System;

namespace Conquest.Assets.Graphics.UtilityGUI
{
	public struct ScheduleEntry
	{
		public readonly float expireTime;
		public readonly GUIManager.GUIState state;
		
		public ScheduleEntry (GUIManager.GUIState State, float ExpireTime)
		{
			state = State;
			expireTime = ExpireTime;
		}
	}
}

