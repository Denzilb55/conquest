using System;

namespace Conquest.Assets.Graphics.UtilityGUI
{
	public class ActionButton
	{
		public System.Action action;
		public string ButtonText { get; private set; }
		
		public ActionButton (System.Action action, string buttonText)
		{
			this.action = action;
			ButtonText = buttonText;
		}
		
		public void PerformAction()
		{
			action();	
		}
		
		public static readonly ActionButton ButtonCloseSpecialMessage;
		
		static ActionButton()
		{
			System.Action action = () =>
			{
				GameGUI.guiManager.CloseSpecialReport();
			};
			
			ButtonCloseSpecialMessage = new ActionButton(action,"Close");
		}
	}
}

