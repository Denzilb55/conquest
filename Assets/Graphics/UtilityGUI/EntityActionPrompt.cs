using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Graphics.UtilityGUI
{
	public class EntityActionPrompt
	{
		public float X {get; private set; }
		public float Y {get; private set; }	
		public List<ActionButton> Buttons {get; private set; } 
		
		public EntityActionPrompt (float x, float y, List<ActionButton> buttons)
		{
			X = x;
			Y = y;
			Buttons = buttons;
		}
	}
}

