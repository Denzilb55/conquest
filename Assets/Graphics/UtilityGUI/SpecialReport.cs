using System;
using System.Collections.Generic;
using UnityEngine;

namespace Conquest.Assets.Graphics.UtilityGUI
{
	public enum Mood {ReportSuccess = 0, Default}
	
	public class SpecialReport
	{
		public string [] SpecialReportMessage {get; private set;}
		private Mood mood;
		private int reportButtonIndex;
		public int [] Spacing { get; private set; }
		public GUIStyle [] Style { get; private set; }
		public List<ActionButton> Buttons { get; private set; } 
		
		public string ButtonText
		{
			get { return SpecialReportButtonText[(int)mood][reportButtonIndex]; }	
		}
		
		public SpecialReport (string [] message, Mood mood, int [] spacing, GUIStyle [] style)
		{
			SpecialReportMessage = message;
			reportButtonIndex = UnityEngine.Random.Range(0, SpecialReportButtonText[(int)mood].Length);
			this.mood = mood;
			Spacing = spacing;
			Style = style;
			Buttons = new List<ActionButton>();
			Buttons.Add(ActionButton.ButtonCloseSpecialMessage);
		}
		
		
		public SpecialReport (string [] message, Mood mood, int [] spacing, GUIStyle [] style, List<ActionButton> buttons)
		{
			SpecialReportMessage = message;
			reportButtonIndex = UnityEngine.Random.Range(0, SpecialReportButtonText[(int)mood].Length);
			this.mood = mood;
			Spacing = spacing;
			Style = style;
			Buttons = buttons;
		}
		
		private static readonly string [][] SpecialReportButtonText;
		
		static SpecialReport()
		{
			SpecialReportButtonText = new string[][]{new string[]{"OK!", "Nice ... now be gone with you!", "That's great news!", "Good. Now stop bothering me!", "Do you want a pat on the back? Get lost!", "It's about time!", "Fine job. Now scurry off before I take your head"}, 
													 new string[]{"OK"} };
		}

	}
}

