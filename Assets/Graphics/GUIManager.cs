using System;
using System.Collections.Generic;
using Conquest.Assets.Graphics.UtilityGUI;
using Conquest.Assets.Entity;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Quests.Rewards;
using Conquest.Assets.Quests;
using UnityEngine;

namespace Conquest.Assets.Graphics
{
	public class GUIManager
	{
		public enum GUIState{None = 0, EndTurnPrompt = 1, ReportActiveTurn = 2, ShowEntityStatus = 4, ReportOutOfPoints = 8, ReportSpecialMessage = 16, PromptEntityAction = 32};
		private GUIState state = GUIState.None;
		private LinkedList<ScheduleEntry> expireList = new LinkedList<ScheduleEntry>();
		public List<Rect> guiBoxes = new List<Rect>();
		public LinkedList<SpecialReport> specialReports = new LinkedList<SpecialReport>();
		
		private EntityActionPrompt actionPrompt;
		
		private Texture2D parchmentTex;
		
		//GUI styles
		private GUIStyle promptStyle;
		private GUIStyle promptStretchTex;
		private GUIStyle StyleActivePlayerReport;
		
		public static GUIStyle titleStyle;
		public static GUIStyle subtitleStyle;
		public static GUIStyle reportStyle;
		
		private static Vector2 scrollPosition = Vector2.zero;	
		

		
		public GUIManager ()
		{
			parchmentTex = (Texture2D)Resources.Load("GUIBar");
			
			promptStyle = new GUIStyle();
			promptStyle.fontSize = 20;
			promptStyle.wordWrap = true;
			promptStyle.normal.textColor = Color.white;
			
			promptStretchTex = new GUIStyle();
			promptStretchTex.stretchHeight = true;
			
			StyleActivePlayerReport = new GUIStyle();
			StyleActivePlayerReport.fontSize = 40;
			StyleActivePlayerReport.normal.textColor = Color.white;
			StyleActivePlayerReport.wordWrap = true;
			
		}
		
		public void Tick()
		{
			LinkedList<ScheduleEntry> removedEntries = new LinkedList<ScheduleEntry>();
		
	
			foreach (ScheduleEntry entry in expireList)
			{
				if (Time.realtimeSinceStartup >= entry.expireTime)
				{
					removedEntries.AddLast(entry);
				}
			}
			
			foreach (ScheduleEntry entry in removedEntries)
			{
				expireList.Remove(entry);	
				UnsetState(entry.state);
			}
		}
		
		public void OnGUI()
		{
			titleStyle = new GUIStyle(GUI.skin.label);
			titleStyle.fontSize = 32;
			titleStyle.wordWrap = true;
			
			subtitleStyle = new GUIStyle(GUI.skin.label);
			subtitleStyle.fontSize = 20;
			subtitleStyle.wordWrap = true;
			
			reportStyle = new GUIStyle(GUI.skin.label);
			reportStyle.fontSize = 14;
			reportStyle.wordWrap = true;	
			
			guiBoxes.Clear();
			RenderGUIs();
	
			//main gui
			Rect mainRect = new Rect (0, Screen.height-80, Screen.width, 80);
			guiBoxes.Add(mainRect);
			GUI.DrawTexture(mainRect,parchmentTex);
			
			GUILayout.BeginArea(new Rect (0, Screen.height-80, Screen.width/3, 80));
			GUILayout.FlexibleSpace();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			if (GUILayout.Button("Quests"))
			{
				if (!IsState(GUIState.ReportSpecialMessage))
				{
					RequestQuestReport();
				}
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.EndArea();
			
			GUILayout.BeginArea(new Rect (Screen.width/3, Screen.height-80, Screen.width/3, 80));
	
			GUILayout.BeginHorizontal();
			int playerPoints = Team.mainPlayer.ActivePoints;
			GUIStyle ppStyle = new GUIStyle(GUI.skin.label);
			ppStyle.fontSize = 16;
			GUILayout.Label("Player Points: " + playerPoints, ppStyle);
			GUILayout.FlexibleSpace();
			GUILayout.BeginVertical ();
				GUILayout.FlexibleSpace();
				GUILayout.Label("Active Player: " + Game.world.turnHandler.ActivePlayer.TeamName);
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("End Turn"))
				{
					if (Game.world.turnHandler.IsMainPlayerActive())
					{
						SetState(GUIState.EndTurnPrompt);
					}
				}
				GUILayout.Space(20);
			GUILayout.EndVertical ();
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
	
			GUILayout.EndArea();
			
			GUILayout.BeginArea(new Rect ((Screen.width*2)/3, Screen.height-80, Screen.width/3, 80));
			GUILayout.EndArea();
		}
		
		public bool IsOnGUI(float x, float y)
		{
			foreach (Rect rect in guiBoxes)
			{

				if (rect.Contains(new Vector2(x, y)))
				{
					return true;	
				}
			}
			return false;
		}
		
		public void StartExpireState(float timePeriod, GUIState state)
		{
			float expireTime = Time.realtimeSinceStartup + timePeriod;
			ScheduleEntry entry = new ScheduleEntry(state, expireTime);
			expireList.AddLast(entry);
			SetState(state);
		}
		
		public void SetSpecialMessage(string [] message, Mood mood, int [] spacing, GUIStyle [] style, List<ActionButton> buttons)
		{
			SpecialReport report = new SpecialReport(message, mood, spacing, style, buttons);
			specialReports.AddLast(report);
			SetState(GUIState.ReportSpecialMessage);
		}
		
		
		public void SetSpecialMessage(string [] message, Mood mood, int [] spacing, GUIStyle [] style)
		{
			SpecialReport report = new SpecialReport(message, mood, spacing, style);
			specialReports.AddLast(report);
			SetState(GUIState.ReportSpecialMessage);
		}
		
		
		/*public void SetSpecialMessageExpire(float timePeriod, string [] message, Mood mood, int [] spacing, GUIStyle [] style)
		{
			float expireTime = Time.realtimeSinceStartup + timePeriod;
			ScheduleEntry entry = new ScheduleEntry(GUIState.ReportSpecialMessage, expireTime);
			expireList.AddLast(entry);
			SetSpecialMessage(message, mood, spacing, style);
		}*/
		
		public void SetState(GUIState value)
		{
			state |= value;	
		}
		
		public void UnsetAllStates()
		{
			state = GUIState.None;
		}
		
		public void UnsetState(GUIState value)
		{
			state &= (~value);	
		}
					
		public bool IsState(GUIState checkState)
		{
			return ((state & checkState) != 0);
		}
		
		private void RenderGUIs()
		{
			if (IsState(GUIState.EndTurnPrompt))
			{
				CommandPrompt();
			} 	
			
			if (IsState(GUIState.ReportActiveTurn))
			{
				ReportActiveTurn();
			}
			
			if (IsState(GUIState.ShowEntityStatus))
			{
				ShowEntityStatus();
			}
			
			if (IsState(GUIState.ReportOutOfPoints))
			{
				ReportOutOfPoints();
			}
			
			if (IsState(GUIState.ReportSpecialMessage))
			{
				ReportSpecialMessage();	
			}
			
			if (IsState(GUIState.PromptEntityAction))
			{
				PromptEntityAction();
			}
	
		}
		
		public void RequestActionPrompt(float x, float y, List<ActionButton> buttons)
		{
			SetState(GUIState.PromptEntityAction);	
			actionPrompt = new EntityActionPrompt(x, y, buttons);
		}
		
		private void PromptEntityAction()
		{
			if (actionPrompt == null) return;
			
			int x = (int)actionPrompt.X;
			int y = (int)actionPrompt.Y;
			
			Rect guiRect = new Rect (x, y, 120, 60);
			guiBoxes.Add(guiRect);

			GUILayout.BeginArea(guiRect);

			foreach (ActionButton button in actionPrompt.Buttons)
			{				
				if (GUILayout.Button(button.ButtonText))
				{
					button.PerformAction();	
				}
			}
		
			GUILayout.EndArea();
		}
		
		public void RequestQuestReport()
		{
			int messageCount = Team.mainPlayer.quests.Count * 3 + 1;
			
			string [] messages = new string[messageCount];
			int [] spacing = new int[messageCount];
			GUIStyle[] styles = new GUIStyle[messageCount];
			
			
			messages[0] = "Quests";
			spacing[0] = 15;
			styles[0] = titleStyle;
			
			int i = 1;
			foreach (QuestBasic quest in Team.mainPlayer.quests)
			{
				messages[i] = "- " + quest.GetQuestTitle();
				spacing[i] = 15;
				styles[i] = subtitleStyle;
				i++;
				messages[i] = quest.GetQuestDescription();
				spacing[i] = 30;
				styles[i] = reportStyle;
				i++;
				messages[i] = quest.GetQuestRewardDescription();
				spacing[i] = 30;
				styles[i] = reportStyle;
				i++;
			}
			
			SetSpecialMessage(messages, Mood.Default, spacing, styles);
		}
		
		
		public void RequestOutOfEnergyReport()
		{
			int messageCount = 1;
			
			string [] messages = new string[messageCount];
			int [] spacing = new int[messageCount];
			GUIStyle[] styles = new GUIStyle[messageCount];
			
			
			messages[0] = "Unit does not have sufficient energy to do this!";
			spacing[0] = 15;
			styles[0] = subtitleStyle;	
			
			SetSpecialMessage(messages, Mood.Default, spacing, styles);
		}
		
		
		private void ReportSpecialMessage()
		{
			SpecialReport report = specialReports.First.Value;

			Rect guiRect = new Rect (Screen.width/2 -200, Screen.height/2 - 150, 400, 300);
			guiBoxes.Add(guiRect);
			GUI.DrawTexture(guiRect,parchmentTex);
			GUILayout.BeginArea(guiRect);
			scrollPosition = GUILayout.BeginScrollView (scrollPosition);
			GUILayout.Space(15);

			for (int i = 0; i < report.SpecialReportMessage.Length; i++)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Space(report.Spacing[i]);
				GUILayout.Label(report.SpecialReportMessage[i], report.Style[i]);
				GUILayout.Space(15);
				GUILayout.EndHorizontal();	
			}
			GUILayout.EndScrollView();
			GUILayout.FlexibleSpace();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			foreach (ActionButton button in report.Buttons)
			{
				if (GUILayout.Button(button.ButtonText))
				{
					button.PerformAction();
				}
			}

			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();	
			GUILayout.Space(15);
			GUILayout.EndArea();
		}
		
		public void CloseSpecialReport()
		{
			specialReports.RemoveFirst();
			if (specialReports.Count == 0)
			{
				UnsetState(GUIState.ReportSpecialMessage);
			}
		}
		
		private void ReportOutOfPoints()
		{
			string reportString = "You have insufficient player points!";
			GUIStyle reportStyle = new GUIStyle(GUI.skin.box);
			reportStyle.fontSize = 30;
			reportStyle.wordWrap = true;
			GUI.Box(new Rect (Screen.width/2 -150, Screen.height/2 - 40, 300, 80), reportString, reportStyle); 
		}
		
		private void ShowEntityStatus()
		{
			Rect guiRect = new Rect (Screen.width -250, Screen.height/2 - 130, 220, 260);
			GUI.DrawTexture(guiRect,parchmentTex);
			GUILayout.BeginArea(guiRect);
			ShowEntityStatus(Game.inputHandler.SelectedEntity);
			GUILayout.EndArea();
		}
		
		private void ShowEntityStatus(IEntity entity)
		{
			//Entity type
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.Label(entity.GetEntityType().ToString());
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			//owner
			GUILayout.BeginHorizontal();
			GUILayout.Space(15);
			GUILayout.Label("Owner: ");
			GUILayout.FlexibleSpace();
			GUILayout.Label(entity.GetOwner().TeamName);
			GUILayout.FlexibleSpace();
			GUILayout.EndHorizontal();
			
			IHealthy healthy = entity as IHealthy;
			
			if (healthy != null)
			{
				int health = healthy.GetHealthModule().healthStats.Health;
				int maxHealth = healthy.GetHealthModule().healthStats.MaxHealth;
				
				GUILayout.BeginHorizontal();
				GUILayout.Space(15);
				GUILayout.Label("Health: ");
				GUILayout.FlexibleSpace();
				GUILayout.Label(health + " / " + maxHealth);
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			
			IEnergetic energetic = entity as IEnergetic;
			
			if (energetic != null)
			{
				int energy = energetic.GetEnergyModule().energyStats.Energy;
				int maxEnergy = energetic.GetEnergyModule().energyStats.MaxEnergy;
				
				GUILayout.BeginHorizontal();
				GUILayout.Space(15);
				GUILayout.Label("Energy: ");
				GUILayout.FlexibleSpace();
				GUILayout.Label(energy + " / " + maxEnergy);
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			
			IAttacker attacker = entity as IAttacker;
			
			if (attacker != null)
			{
				int damage = attacker.GetAttackModule().GetDamage();
				
				GUILayout.BeginHorizontal();
				GUILayout.Space(15);
				GUILayout.Label("Damage: ");
				GUILayout.FlexibleSpace();
				GUILayout.Label(damage.ToString());
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
			}
			
			IItemWielder itemWielder = entity as IItemWielder;
			
			if (itemWielder != null)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Space(15);
				GUILayout.Label("Equipped Items");
				GUILayout.FlexibleSpace();
				GUILayout.EndHorizontal();
				
				foreach (IItemSlotModule slot in itemWielder.GetItemHandlingModule().itemSlots)
				{
					GUILayout.BeginHorizontal();
					GUILayout.Space(15);
					GUILayout.Label(slot.ToString() + ": ");
					GUILayout.FlexibleSpace();
					GUILayout.Label(slot.GetItemLabel());
					GUILayout.FlexibleSpace();
					GUILayout.EndHorizontal();
				}
			}
		}
		
		private void ReportActiveTurn()
		{
			string reportString = "It is now the turn of: " + Game.world.turnHandler.ActivePlayer.TeamName;
			GUIStyle reportStyle = new GUIStyle(GUI.skin.box);
			reportStyle.fontSize = 30;
			reportStyle.wordWrap = true;
			GUI.Box(new Rect (Screen.width/2 -150, Screen.height/2 - 40, 300, 80), reportString, reportStyle); 
		}
		
		private void CommandPrompt()
		{
			Rect promptRect = new Rect (Screen.width/2 -90, Screen.height/2 - 50, 180, 100);
			guiBoxes.Add(promptRect);
			GUI.DrawTexture(promptRect,parchmentTex);
			GUILayout.BeginArea(promptRect);  
				GUILayout.FlexibleSpace();
				GUILayout.Label("Do you wish to end your turn?", promptStyle);
				GUILayout.BeginHorizontal ("box");
					GUILayout.FlexibleSpace();
					if (GUILayout.Button ("Yes"))
					{
						UnsetState(GUIState.EndTurnPrompt);
						Game.inputHandler.EndTurnCommand();
					}
					GUILayout.FlexibleSpace();
				    if (GUILayout.Button ("No"))
					{
						UnsetState(GUIState.EndTurnPrompt);
					}
					GUILayout.FlexibleSpace();
			    GUILayout.EndHorizontal ();
				GUILayout.FlexibleSpace();
			GUILayout.EndArea();
		}
		
		
	}
}

