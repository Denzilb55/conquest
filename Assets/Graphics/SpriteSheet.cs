using System;
using UnityEngine;

namespace Conquest.Assets.Graphics
{
	public class SpriteSheet
	{
		public const int SHEET_ROWS = 50;
		public const int SHEET_COLS = 50;
		
		public const int ICON_WIDTH = 64;
		public const int ICON_HEIGHT = 64;
		
		public const int ICON_SHIFT_WIDTH = 6;
		public const int ICON_SHIFT_HEIGHT = 6;
		
		public int width;
    	public int height;
		public Color32[] sheetPixels;
		
		public Color32[] colors = new Color32[4];
		
		public SpriteSheet (Texture2D texture)
		{
			sheetPixels = texture.GetPixels32();
			width = texture.width;
			height = texture.height;
			
			colors[0] = new Color32(17,17,17,255);
			colors[1] = new Color32(51,51,51,255);
			colors[2] = new Color32(85,85,85,255);
			colors[3] = new Color32(119,119,119,255);
		}
		
		public void Load(Color32[] pixels, Color32[] colors, int index, int maxWidth, int maxHeight)
		{
			Load(pixels, getIndexRow(index), getIndexCol(index), colors, maxWidth, maxHeight, false, false, false);
		}
		
		
		
		private void Load(Color32[] pixels, int row, int col, Color32 []colors, int maxWidth, int maxHeight, bool yMirror, bool xMirror, bool rotate){	

			Color32 pixCol;
	
			row = SHEET_ROWS - row -1;
			
			for (int i = 0; i < maxWidth; i++){
				
				for (int j = 0; j < maxHeight; j++){	
	
	                int pixPos = i + j * (ICON_WIDTH) * SHEET_COLS + (col + row * ICON_HEIGHT * SHEET_COLS) * ICON_WIDTH;
		
					pixCol = sheetPixels[pixPos];
	                if (colors != null)
	                {
	                    
	                    for (int n = 0; n < colors.Length; n++)
	                    {

	                        if (pixCol.Equals(this.colors[n]))
	                        {         
								pixCol = colors[n];
	                            break;
	                        }
							
	                    }
	                }
				
					pixels[i+j*maxWidth] = pixCol; 
				}	
			}
    	}  
	
    
	    public int getIndexCol(int index)
		{
	        return (index % SHEET_ROWS);
	    }
	    
	    public int getIndexRow(int index)
		{
	        return (index / SHEET_ROWS);
	    }
		
		public const int NONE = 0;
		public const int ROTATE_CLOCKWISE = 1;
		public const int ROTATE_ANTI_CLOCKWISE = 2;
		public const int ROTATE_FULL = 3;
		public const int FLIP_X = 4;
		public const int FLIP_Y = 5;
		public const int ROTATE_CW_FLIP_X = 6;
		public const int ROTATE_ACW_FLIP_X = 7;
	}
}

