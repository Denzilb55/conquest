using System;
using UnityEngine;
using System.Collections.Generic;
using Conquest.Assets.Teams;

namespace Conquest.Assets.Graphics
{
	public class Sprite
	{
		private Texture2D texture;
		private int _id;
		public int index;
		private static int oldID;
		
		public Sprite()
		{
			_id = oldID++;
			sprites.Add(this);
		}
		
		private void AddSprite(SpriteSheet spriteSheet, int spriteSheetIndex, Color32 []colors)
	    {
	        Texture2D texture = new Texture2D(SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT, TextureFormat.ARGB32, false);
			this.texture = texture;
			Color32 []pixels = new Color32[texture.height*texture.width];
	        spriteSheet.Load(pixels, colors, spriteSheetIndex, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
		    texture.SetPixels32(pixels);
			texture.Apply();
			index = spriteSheetIndex;
	    }
		
		
		public static List<Sprite> sprites = new List<Sprite>();
		public static int atlasDimensions = 32;
		public static Texture2D textureAtlas = new Texture2D(atlasDimensions * SpriteSheet.ICON_WIDTH, atlasDimensions * SpriteSheet.ICON_HEIGHT, TextureFormat.ARGB32, false);
		
		public static readonly Sprite ColorSelector = new Sprite();
		public static readonly Sprite Grass = new Sprite();
		public static readonly Sprite [] Soldier = new Sprite[Team.TEAM_COUNT];
		public static readonly Sprite [] Bandit = new Sprite[Team.TEAM_COUNT];
		public static readonly Sprite [] BountyHunter = new Sprite[Team.TEAM_COUNT];
		public static readonly Sprite House = new Sprite();
		public static readonly Sprite SelectedBox = new Sprite();
		public static readonly Sprite MovementIndicator = new Sprite();
		public static readonly Sprite Dirt = new Sprite();
		public static readonly Sprite Water = new Sprite();
		public static readonly Sprite Sand = new Sprite();
		public static readonly Sprite Tree = new Sprite();
		public static readonly Sprite Brush = new Sprite();
		public static readonly Sprite Rock = new Sprite();
		public static readonly Sprite Ice = new Sprite();
		public static readonly Sprite Unexplored = new Sprite();
		
		public static readonly Sprite AttackMarker = new Sprite();
		public static readonly Sprite SelectMarker = new Sprite();
		
		static void BuildAtlas ()
		{
			int count = 0;
			Color32 [] atlasPixels = textureAtlas.GetPixels32();
			
			foreach (Sprite s in sprites)
			{
				Texture2D text = s.texture;

				Color32 [] pixels = text.GetPixels32();
		
				for (int i = 0; i < SpriteSheet.ICON_WIDTH; i++)
				{
					for (int j = 0; j < SpriteSheet.ICON_HEIGHT; j++)
					{
						atlasPixels[i + j * SpriteSheet.ICON_WIDTH * atlasDimensions + getIndexRow(count, atlasDimensions) * SpriteSheet.ICON_WIDTH * SpriteSheet.ICON_HEIGHT * atlasDimensions + getIndexCol(count, atlasDimensions) * SpriteSheet.ICON_HEIGHT] = pixels[i + j * SpriteSheet.ICON_WIDTH];
					}
				}
				
				s.index = count;
			
				count++;
				s.texture = null;
			}
			
			textureAtlas.SetPixels32(atlasPixels);
			textureAtlas.Apply();
		}
		
		
		public static void InitializeSprites(SpriteSheet spriteSheet)
		{
			ColorSelector.AddSprite(spriteSheet,0,Team.teams[0].color);
			Grass.AddSprite(spriteSheet,1,null);
			House.AddSprite(spriteSheet,3,null);
			SelectedBox.AddSprite(spriteSheet,4,null);
			MovementIndicator.AddSprite(spriteSheet,5,null);
			Dirt.AddSprite(spriteSheet,6,null);
			Water.AddSprite(spriteSheet,7,null);
			Sand.AddSprite(spriteSheet,8,null);
			Tree.AddSprite(spriteSheet,9,null);
			Brush.AddSprite(spriteSheet,10,null);
			Rock.AddSprite(spriteSheet,11,null);
			Ice.AddSprite(spriteSheet,12,null);
			AttackMarker.AddSprite(spriteSheet, 14, null);
			SelectMarker.AddSprite(spriteSheet, 15, null);
			Unexplored.AddSprite(spriteSheet, 17, null);
			
			for (int i = 0; i < Team.TEAM_COUNT; i++)
			{
				Soldier[i].AddSprite(spriteSheet,2,Team.teams[i].color);
				Bandit[i].AddSprite(spriteSheet,13,Team.teams[i].color);
				BountyHunter[i].AddSprite(spriteSheet, 16, Team.teams[i].color);
			}
			
			
			/*
			 * Build a pixel perfect Texture Atlas
			 */
			BuildAtlas ();
		}
		
	
		
		static Sprite()
		{
			for (int i = 0; i < Team.TEAM_COUNT; i++)
			{
				Soldier[i] = new Sprite();	
				Bandit[i] = new Sprite();
				BountyHunter[i] = new Sprite();
			}
		}
		
		public static int getIndexCol(int index, int atlasCols)
	    {
	        return (index % atlasCols);
	    }
	
	    public static int getIndexRow(int index, int atlasCols)
	    {
	        return (index / atlasCols);
	    }
	}
	
	
}

