using UnityEngine;
using System.Collections.Generic;
using Conquest;
using Conquest.Assets.Graphics.UtilityGUI;
using Conquest.Assets.Entity;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Graphics;

public class GameGUI : MonoBehaviour {

	public GUITexture guiBar;
	
	public const int GUI_HEIGHT = 100;
	
	
	public static GUIManager guiManager;
	

	
	
	
	// Use this for initialization
	void Start () 
	{
		this.guiTexture.pixelInset = new Rect(0, 0, Screen.width, GUI_HEIGHT);
		
	}
	
	void Awake()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (guiManager != null)
			guiManager.Tick();
	}
	

	void OnGUI()
	{
		if (guiManager != null)
			guiManager.OnGUI();
	}
	
	
	
	
	
	
}
