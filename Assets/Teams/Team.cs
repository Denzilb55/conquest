using System;
using UnityEngine;
using System.Collections.Generic;
using Conquest.Assets.Entity;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity.Stats.Template;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Quests;
using Conquest.Assets.Graphics;
using System.Linq;

namespace Conquest.Assets.Teams
{
	public class Team
	{
		public const int COLOR_COUNT = 2;
		private static int oldID;
		
		public Color32 [] color = new Color32[COLOR_COUNT];
		public int ID;
		public List<Team> allies = new List<Team>();
		public int ActivePoints { get; protected set; }
		public int DefaultActivePoints { get; set; }
		
		public List<QuestBasic> quests = new List<QuestBasic>();
		
		public String TeamName
		{
			get; private set;	
		}
		
		public Team (Color32 color1, Color32 color2, String Name)
		{
			color[0] = color1;
			color[1] = color2;
			teams[oldID] = this;
			this.ID = oldID++;
			DefaultActivePoints = 15;
			ActivePoints = DefaultActivePoints;
			TeamName = Name;
		}
		
		public List<IEntity> GetEntities()
		{
			return Game.world.entityManager.GetEntitiesOfTeam(this);
		}
		
		public void AcceptQuest(World world, QuestBasic quest)
		{
			quests.Add(quest);	
			quest.AcceptCallback(world);
			quest.SetOwner(this);			
		}
		
		public bool TryReduceActivePoints(int value)
		{
			if (ActivePoints >= value)
			{
				ActivePoints -= value;
				return true;
			}
			
			if (this == mainPlayer)
			{
				Game.guiManager.StartExpireState(3f, GUIManager.GUIState.ReportOutOfPoints);
			}
			return false;
		}
		
		public void RewardPlayerPoints(int value)
		{
			ActivePoints += value;	
		}
		
		
		public virtual void StartTurn(World world)
		{			
			ActivePoints = DefaultActivePoints;
			foreach(var entity in world.entityManager.GetEntitiesOfTeam(this))
			{
				entity.StartTurn(world);
			}		
		}
		
		
		public override string ToString ()
		{
			return TeamName;
		}
		
		public static implicit operator int(Team instance)
		{
			return instance.ID;	
		}		
		
		public const int TEAM_COUNT = 2;
		public static readonly Team [] teams = new Team[TEAM_COUNT];
		public static readonly Team mainPlayer;
	//	public static readonly Team enemyPlayer;
		public static readonly TeamAI neutralPlayer;
		
		static Team()
		{
			mainPlayer = new Team(new Color32(50,50,100,255),new Color32(0,0,200,255), "You");
		//	enemyPlayer = new TeamAI(new Color32(100,50,50,255),new Color32(200,0,0,255), "Enemy");
			neutralPlayer = new TeamAI(new Color32(70,70,70,255),new Color32(200,200,200,255), "Neutral");
		}
	}
}

