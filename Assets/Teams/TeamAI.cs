using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Teams.MovesAI;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.Entity;
using System.Threading;
using System.Linq;
using Conquest.Assets.Teams.UtilityAI;


namespace Conquest.Assets.Teams
{
	public class TeamAI: Team, IDestroyListener, IMoveListener
	{
		
		private int timer;
		private const int MAX_TIMER = 60;
		private bool thinking = false;
		private bool strategizing = false;
		
		
		private List<IEntity> priorityTargets;
		List<RelationalPriorityTableEntry> priorityTable;
		private LinkedList<IMove> moves;
		
		private IMove ActiveMove
		{
			get { return moves.First.Value; }	
		}
		
		public TeamAI (Color32 color1, Color32 color2, String Name): base (color1, color2, Name)
		{
			priorityTargets = new List<IEntity>();
			moves = new LinkedList<IMove>();
			DefaultActivePoints = 30;
			priorityTable = new List<RelationalPriorityTableEntry>();

		}
		
		public void AddPriorityTarget(IEntity entity)
		{
			priorityTargets.Add(entity);
		}
		
		public override void StartTurn(World world)
		{
			base.StartTurn(world);
			timer = 0;
			thinking = true;
			priorityTable = ConstructPriorityTable(world);
			SortPriorityTable();
			DoStrategize(world);
			UnityEngine.Debug.Log("DONE INITIAL");
		}
		
		public void AddMove(IMove move)
		{
			moves.AddLast(move);	
		}
		
		public List<RelationalPriorityTableEntry> ConstructPriorityTable(World world)
		{
			List<RelationalPriorityTableEntry> priorityTable = new List<RelationalPriorityTableEntry>();
			
			foreach (IEntity target in priorityTargets)
			{
				List<IEntity> availableEntities = world.entityManager.GetEntitiesAroundCoordinate(target.GetWorldX(), target.GetWorldY(), this);
				
				foreach (IEntity hunter in availableEntities)
				{
					if (hunter.GetStance() == StanceType.NeutralPassive) continue;
					
					int dist = hunter.GetManhattenDistanceTo(target.GetWorldX(), target.GetWorldY());
					if (dist < 25)
					{
						RelationalPriorityTableEntry entry = new RelationalPriorityTableEntry(target, hunter, 100 - dist/2f, dist);
						priorityTable.Add(entry);
					}
				}
			}
			
			return priorityTable;
		}
		
		private void SortPriorityTable()
		{
			var sortedList = from element in priorityTable
							 orderby element.Score descending
							 select element;
			
			priorityTable = sortedList.ToList();
		}
		
		private void Strategize(object obj)
		{
			World world = (World)obj;
			List<IEntity> removeHunters = new List<IEntity>();
		
			
			foreach (RelationalPriorityTableEntry entry in priorityTable)
			{
				IEntity target = entry.Target;

				IAttacker hunter = entry.Hunter as IAttacker;
				
				if (hunter != null)
				{
					if (hunter.GetAttackModule().HasSufficientActivePoints() && hunter.GetAttackModule().HasSufficientEnergy())
					{
						if (hunter.GetAttackModule().IsDistanceInRange(entry.Distance) )
						{
							AddMove(new MoveRelentlessAttack(hunter, target));
							goto REMOVALS;
						}
					}
					else
					{
						removeHunters.Add(hunter);
					}
				}

			}
			
			foreach (RelationalPriorityTableEntry entry in priorityTable)
			{
				IEntity target = entry.Target;
				
				if (removeHunters.Contains(target)) continue;
				
				//retryTranslate = false;
				if (ActivePoints < 1) break;
				IEnergeticMovable movable = entry.Hunter as IEnergeticMovable;
			
				if (movable != null)
				{

					MoveNode mz = movable.FindMovableZone(world);
					
					/*if (mz.BranchCount == 0)
					{
						removeHunter = entry.Hunter;
						retryTranslate = true;
						break;
					}*/
					
					MoveNode closestNode = mz.FindClosestTo(target.GetWorldX(),target.GetWorldY());
					if (target.GetManhattenDistanceTo(closestNode.coords.X, closestNode.coords.Y) < entry.Distance)
					{
						LinkedList<MoveNode> path = MoveNode.ExtractPathFromMovementZone(closestNode);
							
						if (path.First != null && path.First.Value != mz)
						{
							AddMove(new MoveTranslate(movable, path));
							goto REMOVALS;
						}
					}
					else
					{
						removeHunters.Add(entry.Hunter);
						continue;
					}

					
				}

			}
		
		REMOVALS:
			foreach (var hunter in removeHunters)
			{
				priorityTable.RemoveAll( o => o.Hunter == hunter);	
			}
			
			
			strategizing = false;
			
			if (moves.Count == 0)
			{
				UnityEngine.Debug.Log("Stop thinking");
				thinking = false;	
			}
		}
		
		private void DoStrategize(World world)
		{
			if (!strategizing)
			{
				strategizing = true;
				Thread thread = new Thread(new ParameterizedThreadStart(Strategize));
				thread.Start(world);
			}
		}
		
		public void Tick(World world)
		{
			if (strategizing || Game.inputHandler.movementAnimator.IsAnimating()) return;
			
			if (ActivePoints == 0 || !thinking)
			{
				if (timer++ > MAX_TIMER)
				{
					UnityEngine.Debug.Log("REMAINING: " + ActivePoints);
					world.turnHandler.CycleTurn(world);	
				}
				
				thinking = false;
				return;
			}
			
			
			if (moves.Count > 0 )
			{
				
				if (ActiveMove.FocusAndWait(world) && ActiveMove.ExecuteMove(world))
				{
					ActiveMove.FocusAndWait(world);
					moves.RemoveFirst();
				}		
			}
			else
			{
				
				if (thinking)
				{
					DoStrategize(world);
					return;
				}
			}
			
			
		}
		

		#region IDestroyListener implementation
		public void NotifyDestroy (Conquest.World world, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity, Conquest.Assets.Entity.Modules.Interfaces.IEntity destroyer)
		{
			priorityTargets.Remove(entity);
			
			if (world.turnHandler.ActivePlayer == this)
			{
				priorityTable.RemoveAll( o => o.Target == entity);	
				
				if (entity.GetOwner() == this)
				{
					priorityTable.RemoveAll( o => o.Hunter == entity);
				}
			}
			
		}
		#endregion

		#region IMoveListener implementation
		public void NotifyMove (Conquest.World world, Conquest.Assets.Entity.Modules.Interfaces.IEntity movedEntity)
		{
			if (movedEntity.GetOwner() != this) return;
			
			foreach (var entry in priorityTable)
			{
				if (entry.Hunter == movedEntity)
				{
					entry.Distance = movedEntity.GetManhattenDistanceTo(entry.Target.GetWorldX(), entry.Target.GetWorldY());
					entry.Score = 100 - entry.Distance*0.5f;
				}
			}
			
			SortPriorityTable();
		}
		#endregion
	}
}

