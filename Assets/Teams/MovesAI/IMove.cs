using System;

namespace Conquest.Assets.Teams.MovesAI
{
	public interface IMove
	{
		bool ExecuteMove(World world);
		bool FocusAndWait(World world);
		void ResetWaitTimer();
	}
}

