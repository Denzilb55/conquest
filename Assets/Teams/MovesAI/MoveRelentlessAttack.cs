using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Teams.MovesAI
{
	public class MoveRelentlessAttack: MoveBase
	{
		public IAttacker Entity { get; private set; }
		public IEntity Target { get; private set; }
		
		public MoveRelentlessAttack (IAttacker entity, IEntity target)
		{
			Entity = entity;
			Target = target;
		}
		
		public override bool ExecuteMove(World world)
		{
			if (!completed)
			{
				while (!Target.IsDestroyed() && Entity.GetAttackModule().DoModuleCommandAction(world, null, Target));
				completed = true;
				ResetWaitTimer();
			}
			
			if (completed && FocusAndWait(world))
			{
				return true;
			}
			else
			{
				return false;	
			}
		}
		
		#region implemented abstract members of Conquest.Assets.Teams.MovesAI.MoveBase
		protected override bool MustFocus (World world)
		{
			return true;
		}

		protected override int GetFocusX ()
		{
			return Entity.GetWorldX();
		}

		protected override int GetFocusY ()
		{
			return Entity.GetWorldY();
		}
		#endregion
	}
}

