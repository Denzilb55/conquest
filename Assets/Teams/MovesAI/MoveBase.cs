using System;

namespace Conquest.Assets.Teams.MovesAI
{
	public abstract class MoveBase: IMove
	{
		private const int TIMER_COUNT = 30;
		private int timer = TIMER_COUNT;
		protected bool completed = false;
		
		public MoveBase ()
		{
		}

		#region IMove implementation
		public abstract bool ExecuteMove (Conquest.World world);
	
		protected abstract bool MustFocus(World world);
		protected abstract int GetFocusX();
		protected abstract int GetFocusY();
		
		public bool FocusAndWait (World world)
		{
			if (MustFocus(world))
			{
				world.FocusViewportOnTile(GetFocusX(), GetFocusY());
				
				if (timer-- > 0)
				{
					return false;	
				}
			}
			
			return true;
		}
		
		public void ResetWaitTimer()
		{
			timer = TIMER_COUNT;
		}
		#endregion
	}
}

