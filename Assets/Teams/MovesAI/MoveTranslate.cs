using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Movement;

namespace Conquest.Assets.Teams.MovesAI
{
	public class MoveTranslate: MoveBase, IMovementCallback
	{
		public IEnergeticMovable Entity { get; private set; }
		public LinkedList<MoveNode> Path { get; private set; } 
		
		public MoveTranslate (IEnergeticMovable entity, LinkedList<MoveNode> path)
		{
			Entity = entity;
			Path = path;
		}
		
		public override bool ExecuteMove(World world)
		{
			EntityMovementAnimator moveAnim = Game.inputHandler.movementAnimator;
			if (!moveAnim.IsAnimating() && !completed)
			{
				int playerPointCost = 1;
				if (!Entity.GetOwner().TryReduceActivePoints(playerPointCost)) return true;
				
				moveAnim.StartMove(Entity, Path, this);
			}
			
			if (completed && FocusAndWait(world))
			{
				return true;
			}
			else
			{
				return false;	
			}
		}

		#region IMovementCallback implementation
		public void MovementEnd ()
		{
			completed = true;
			ResetWaitTimer();
		}
		#endregion


		#region implemented abstract members of Conquest.Assets.Teams.MovesAI.MoveBase
		protected override bool MustFocus (World world)
		{
			return world.explored[Entity.GetWorldX(), Entity.GetWorldY()];
		}

		protected override int GetFocusX ()
		{
			return Entity.GetWorldX();
		}

		protected override int GetFocusY ()
		{
			return Entity.GetWorldY();
		}
		#endregion
	}
}

