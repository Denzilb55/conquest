using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Teams.UtilityAI
{
	public class RelationalPriorityTableEntry
	{
		public IEntity Target { get; private set; }
		public IEntity Hunter { get; private set; }
		public float Score { get; set; }
		public int Distance { get; set; }
		
		public RelationalPriorityTableEntry(IEntity target, IEntity hunter, float score, int distance)
		{
			Target = target;	
			Hunter = hunter;
			Score = score;
			Distance = distance;
		}
	}
}

