using System;
using System.Collections.Generic;
using Conquest.Assets.Tiles;
using Conquest.Assets.UtilityGeneral;

namespace Conquest.Assets.Environments
{
	public class Environment
	{
		private List<Tile> Tiles;
		private string name;
		
		public int TileCount
		{
			get { return Tiles.Count; }	
		}
		
		public int ID {get; private set; }
		private double persistence;
		private double frequency;
		private double amplitude;
		private int octaves;
		private double offset;
		private int randomSeed;
		
		private PerlinNoise environmentDescriptor;
		
		public double Persistence
		{
			get {return persistence;}	
		}
		
		public double Frequency
		{
			get {return frequency;}	
		}
		
		
		
		public PerlinNoise EnvironmentDescriptor
		{
			get 
			{ 
				if (environmentDescriptor == null)
				{
					SeedNewEnvironmentDescriptor();	
				}
				return environmentDescriptor; 
			}	
		}
		
		public Environment (string name, double persistence, double frequency, double amplitude, int octaves, double offset, List<Tile> tiles)
		{
			this.name = name;
			
			this.persistence = persistence;
			this.frequency = frequency;
			this.amplitude = amplitude;
			this.octaves = octaves;
			this.offset = offset;
			
			if (tiles == null)
			{
				Tiles = new List<Tile>();
			}
			else
			{
				Tiles = tiles;
			}
			
			ID = environments.Count;
			environments.Add(this);
		}
		
		public void AddTile(Tile tile)
		{
			Tiles.Add(tile);	
		}
		
		public void AddTile(Tile tile, int width)
		{
			for (int i = 0; i < width; i++)
			{
				Tiles.Add(tile);
			}
		}
		
		
		public bool Contains(Tile tile)
		{
			foreach (Tile t in Tiles)
			{
				if (t == tile)
				{
					return true;	
				}
			}
			
			return false;
		}
		
		public Tile GetTile(int index)
		{
			return Tiles[index];	
		}
		
		public int GetTileIDAt(int x, int y)
		{
			double pnHeight = EnvironmentDescriptor.GetHeight(x,y);
					
			int ID = (int)(pnHeight);
			if (ID < 0)
			{
				ID = 0;
			}
			if (ID >= TileCount)
			{
				ID = TileCount - 1;
			}
		
			return GetTile(ID).ID;
		}
		
		
		
		public void SeedNewEnvironmentDescriptor()
		{
			SeedNewEnvironmentDescriptor(UnityEngine.Random.Range(-10000, 10000));	
		}
		
		public void SeedNewEnvironmentDescriptor(int randomseed)
		{
			if (randomseed != randomSeed)
			{
				randomSeed = randomseed;
				environmentDescriptor = new PerlinNoise(persistence,frequency,amplitude*TileCount,octaves,randomseed,amplitude*TileCount/2f + offset);
			}
		}
		
		public int GetTileHeight(Tile tile)
		{
			int a = 0;
			int c = 0;
			
			foreach (Tile t in Tiles)
			{
				if (t != tile)
				{
					if (c != 0) break;
					a++;
				}
				else
				{
					c++;
				}
			}
			
			return (int)((a + c/2)*amplitude);
		}
		
		public override string ToString ()
		{
			return name;
		}
		
		public static readonly Environment Arctic = new Environment("Arctic", 1f, 1f, 1f, 1,0, null);
		public static readonly Environment DeepOcean = new Environment("Deep Ocean", 1f, 1f, 1f, 1,0, null);
		public static readonly Environment Oceanic = new Environment("Oceanic", 0.25f, 0.09f, 1, 6, 0, null);  //needs work probably
		public static readonly Environment Beach = new Environment("Beach", 0.5f, 0.5f, 1,1,0, null);
		public static readonly Environment DustyField = new Environment("Dusty Field", 0.8f, 0.2f, 3, 7, 0, null);
		public static readonly Environment PlainField = new Environment("Plain Field", 0.85f, 1.4f, 1.2f, 7, -1.2f, null);
		public static readonly Environment IslandLake = new Environment("Island Lake", 0.3f, 0.3f, 1f, 3, -0.2f, null);  //maybe remove
		public static readonly Environment Jungle = new Environment("Jungle", 0.75f, 0.6f, 1f, 4,0, null);
		public static readonly Environment DenseForest = new Environment("Dense Forest", 0.75f, 0.3f, 1f, 8,-0.25f, null);
		public static readonly Environment Desert = new Environment("Desert", 1f, 1f, 1f, 1,0, null); 
		public static readonly Environment GrassPlain = new Environment("Grass Plains", 0.5f, 0.3f,1,3,0,null);
		public static readonly Environment Riverbank = new Environment("Riverbank", 0.6f, 0.001f,1,8,0,null);
		public static readonly Environment RiverMarsh = new Environment("River Marsh", 0.3f, 0.1f,1,2,0,null);
		public static readonly Environment River = new Environment("River", 0.2f,0.008f,1f,2,0,null);
		public static readonly Environment DustPlains = new Environment("Dust Plains", 0.6f, 0.5f,1,3,0,null);
				
		
		public static readonly Environment Continent = new Environment("Continent", 0.4f, 0.02f, 1f, 8, 0f, null);

		
		public List<Environment> environments = new List<Environment>();
		
		static Environment()
		{
			DustPlains.AddTile(Tile.Dirt);
			DustPlains.AddTile(Tile.Sand);
			
			River.AddTile(Tile.Water,6);
			River.AddTile(Tile.Dirt,1);
			
			Riverbank.AddTile(Tile.Dirt,4);
			Riverbank.AddTile(Tile.Grass,1);
			
			RiverMarsh.AddTile(Tile.Dirt);
			RiverMarsh.AddTile(Tile.Water,2);
			RiverMarsh.AddTile(Tile.Dirt);
			RiverMarsh.AddTile(Tile.Grass,2);
			RiverMarsh.AddTile(Tile.Tree,1);
			RiverMarsh.AddTile(Tile.Dirt);
			RiverMarsh.AddTile(Tile.Water,2);
			RiverMarsh.AddTile(Tile.Dirt);
			RiverMarsh.AddTile(Tile.Brush,2);
			RiverMarsh.AddTile(Tile.Grass,1);
			
			GrassPlain.AddTile(Tile.Grass,7);
			GrassPlain.AddTile(Tile.Tree,1);
			GrassPlain.AddTile(Tile.Grass,4);
			GrassPlain.AddTile(Tile.Brush,1);
			GrassPlain.AddTile(Tile.Grass,8);
			
			Desert.AddTile(Tile.Sand);
			DenseForest.AddTile(Tile.Tree,1);	
			DenseForest.AddTile(Tile.Grass,1);	
			Arctic.AddTile(Tile.Ice);
			DeepOcean.AddTile(Tile.Water);
			Beach.AddTile(Tile.Sand);
			
			Jungle.AddTile(Tile.Brush,3);
			Jungle.AddTile(Tile.Dirt);
			Jungle.AddTile(Tile.Tree,3);
			Jungle.AddTile(Tile.Grass,2);
			Jungle.AddTile(Tile.Tree,2);
			
			Oceanic.AddTile(Tile.Ice,5);
			Oceanic.AddTile(Tile.Water,7);

			
			DustyField.AddTile(Tile.Grass);
			DustyField.AddTile(Tile.Tree);
			DustyField.AddTile(Tile.Dirt);
			
			PlainField.AddTile(Tile.Grass);
			PlainField.AddTile(Tile.Tree);
			PlainField.AddTile(Tile.Dirt);
			
			IslandLake.AddTile(Tile.Sand);
			IslandLake.AddTile(Tile.Water,3);
			IslandLake.AddTile(Tile.Dirt);
			IslandLake.AddTile(Tile.Grass);
			IslandLake.AddTile(Tile.Tree);
			
			
			
			
			Continent.AddTile(Tile.Ice,7);
			Continent.AddTile(Tile.Water,14);
			Continent.AddTile(Tile.Sand,2);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Rock);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Grass,2);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Water,2);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Grass);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Tree,2);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Sand,7);
			Continent.AddTile(Tile.Rock);
			Continent.AddTile(Tile.Sand);
			Continent.AddTile(Tile.Sand);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Sand);
			Continent.AddTile(Tile.Dirt);
			Continent.AddTile(Tile.Rock);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Rock);
			Continent.AddTile(Tile.Tree);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Water);
			Continent.AddTile(Tile.Brush);
			Continent.AddTile(Tile.Rock,2);

			
		}
		
		
	}
}

