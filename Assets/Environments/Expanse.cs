using System;
using System.Collections.Generic;
using Conquest.Assets.UtilityGeneral;

namespace Conquest.Assets.Environments
{
	public class Expanse
	{
		private List<Environment> environments;
		
		public int EnvironmentCount
		{
			get { return environments.Count; }	
		}
		
		private double persistence;
		private double frequency;
		private double amplitude;
		private int octaves;
		private double offset;
		private int randomSeed;
		
		private PerlinNoise expanseDescriptor;
		
		public double Persistence
		{
			get {return persistence;}	
		}
		
		public double Frequency
		{
			get {return frequency;}	
		}
		
		
		
		public PerlinNoise ExpanseDescriptor
		{
			get 
			{ 
				if (expanseDescriptor == null)
				{
					SeedNewExpanseDescriptor();	
				}
				return expanseDescriptor; 
			}	
		}
		
		public Expanse (double persistence, double frequency, double amplitude, int octaves, double offset, List<Environment> environments)
		{
			this.persistence = persistence;
			this.frequency = frequency;
			this.amplitude = amplitude;
			this.octaves = octaves;
			this.offset = offset;
			
			if (environments == null)
			{
				this.environments = new List<Environment>();
			}
			else
			{
				this.environments = environments;
			}
		}
		
		public void Add(Environment env)
		{
			environments.Add(env);	
		}
		
		public void Add(Environment env, int width)
		{
			for (int i = 0; i < width; i++)
			{
				environments.Add(env);	
			}
		}
		
		public Environment GetEnvironmentAt(int x, int y)
		{
			double pnHeight = ExpanseDescriptor.GetHeight(x,y);
					
			int ID = (int)(pnHeight);
			if (ID < 0)
			{
				ID = 0;
			}
			if (ID >= EnvironmentCount)
			{
				ID = EnvironmentCount - 1;
			}
		
			return environments[ID];
		}
		
		public int GetTileIDAt(int x, int y)
		{
			Environment env = GetEnvironmentAt(x, y);
			
			return env.GetTileIDAt(x, y);
		}
		
		public void SeedNewExpanseDescriptor()
		{
			SeedNewExpanseDescriptor(UnityEngine.Random.Range(-10000, 10000));	
		}
		
		public void SeedNewExpanseDescriptor(int randomseed)
		{
			if (randomseed != randomSeed)
			{
				randomSeed = randomseed;
				expanseDescriptor = new PerlinNoise(persistence,frequency,amplitude*EnvironmentCount,octaves,randomseed,amplitude*EnvironmentCount/2f + offset);
			}
		}
		
		
		public static readonly Expanse Eden = new Expanse(0.1f, 0.006f, 1f, 2,0,null);
		
		static Expanse()
		{
			Eden.Add(Environment.Arctic,20);
			Eden.Add(Environment.Oceanic,16);
			Eden.Add(Environment.DeepOcean,25);
			Eden.Add(Environment.Beach,2);
			Eden.Add(Environment.DustyField,7);
			Eden.Add(Environment.PlainField,30);
			Eden.Add(Environment.GrassPlain,10);
			Eden.Add(Environment.Jungle, 40);
			Eden.Add(Environment.DenseForest, 14);
			Eden.Add(Environment.Jungle, 10);
			Eden.Add(Environment.Riverbank,3);
			Eden.Add(Environment.River,3);
			Eden.Add(Environment.Riverbank,3);
			Eden.Add(Environment.GrassPlain,25);
			Eden.Add(Environment.PlainField,20);
			Eden.Add(Environment.DustyField,10);
			Eden.Add(Environment.DustPlains, 18);
			Eden.Add(Environment.Desert,44);
			Eden.Add(Environment.DustPlains, 12);
			Eden.Add(Environment.DustyField,10);
			Eden.Add(Environment.PlainField,20);
			Eden.Add(Environment.IslandLake,30);
			Eden.Add(Environment.RiverMarsh,35);
			Eden.Add(Environment.GrassPlain,10);
		}
		
	}
}

