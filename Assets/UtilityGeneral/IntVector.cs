using System;

namespace Conquest.Assets.UtilityGeneral
{
	public class IntVector
	{
		public int X {get; set;}
		public int Y {get; set;}
		
		public IntVector (int x, int y)
		{
			X = x;
			Y = y;
		}
	}
}

