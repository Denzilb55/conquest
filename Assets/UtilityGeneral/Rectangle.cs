using System;

namespace Conquest.Assets.UtilityGeneral
{
	public class Rectangle
	{
		public int PosX {get; private set;}
		public int PosY {get; private set;}
		public int Width {get; private set;}
		public int Height {get; private set;}
		
		public Rectangle (int posX, int posY, int width, int height)
		{
			this.PosX = posX;
			this.PosY = posY;
			this.Width = width;
			this.Height = height;
		}
	}
}

