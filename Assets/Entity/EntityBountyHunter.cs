using System;
using Conquest.Assets.Teams;
using Conquest.Assets.Graphics;
using Conquest.Assets.Entity.Stats.Template;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity
{
	public class EntityBountyHunter : EntityBasicMelee, ICommunicator
	{
		protected CommunicationModule comModule;
		
		public EntityBountyHunter (int PosX, int PosY, Team owner, StanceType stance) : base(PosX, PosY, owner, stance)
		{
			movementModule = new MovementModule(this);
			activeModules.Add(movementModule);
			
			attackModule = new AttackModule(this, EntityTemplate.BountyHunter[Owner].attackerStats);
			interactiveModules.Add(attackModule);
			
			healthModule = new HealthModule(this, EntityTemplate.BountyHunter[Owner].healthStats);
			turnTriggerModules.Add(healthModule);
			
			defenderModule = new DefensiveModule(this);
			energyModule = new EnergyModule(this, EntityTemplate.BountyHunter[Owner].energyStats);
			turnTriggerModules.Add(energyModule);
			
			ItemSlotModule<ISword> swordModule = new ItemSlotModule<ISword>(this, "Skillful Sword-Hand");
			itemHandlerModule.Add(swordModule);
			itemHandlerModule.Equip(Item.LongSword);
			
			if (Owner == Team.neutralPlayer)
			{
				comModule = new CommunicationModule(this, Game.world);
				triggerModules.Add(comModule);
			}
		}

		public override IEntityTemplate GetEntityTemplate ()
		{
			return EntityTemplate.BountyHunter[Owner];
		}

		public override EntityType GetEntityType ()
		{
			return EntityType.BountyHunter;
		}

		public override Conquest.Assets.Graphics.Sprite GetSprite ()
		{
			return Sprite.BountyHunter[Owner];
		}

		#region ICommunicator implementation
		public bool CanCommunicate ()
		{
			return comModule != null;
		}
	
		public Conquest.Assets.Entity.Modules.CommunicationModule GetCommunicationModule ()
		{
			return comModule;
		}
		#endregion
	}
}

