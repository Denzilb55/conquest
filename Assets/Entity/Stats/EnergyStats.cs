using System;

namespace Conquest.Assets.Entity.Stats
{
	public struct EnergyStats
	{
		public readonly int Energy;
		public readonly int MaxEnergy;
				
		public EnergyStats (int energy, int maxEnergy)
		{
			Energy = energy;
			MaxEnergy = maxEnergy;
		}
		
		public EnergyStats (int maxEnergy)
		{
			Energy = maxEnergy;
			MaxEnergy = maxEnergy;
		}
		
	}
}

