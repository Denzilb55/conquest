using System;

namespace Conquest.Assets.Entity.Stats
{
	public struct HealthStats
	{
		public readonly int Health;
		public readonly int MaxHealth;
				
		public HealthStats (int health, int maxHealth)
		{
			Health = health;
			MaxHealth = maxHealth;
		}
		
		public HealthStats (int maxHealth)
		{
			Health = maxHealth;
			MaxHealth = maxHealth;
		}
	}
}

