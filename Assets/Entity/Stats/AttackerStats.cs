using System;

namespace Conquest.Assets.Entity.Stats
{
	public struct AttackerStats
	{
		public readonly int Damage;
		public readonly int EnergyCost;
		public readonly int ActivePointCost;
		
		public AttackerStats (int damage, int energyCost, int activePointCost)
		{
			Damage = damage;
			EnergyCost = energyCost;
			ActivePointCost = activePointCost;
		}
		
	}
}

