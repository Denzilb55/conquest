using System;

namespace Conquest.Assets.Entity.Stats.Template
{
	public class TemplateBountyHunter : IEntityTemplate
	{
		public HealthStats healthStats {get; private set;}
		public EnergyStats energyStats {get; private set;}
		public AttackerStats attackerStats {get; private set;}
		
		public TemplateBountyHunter ()
		{
			healthStats = new HealthStats(100);
			energyStats = new EnergyStats(22);
			attackerStats = new AttackerStats(10,3,1);
		}
	}
}

