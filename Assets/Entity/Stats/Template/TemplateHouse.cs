using System;

namespace Conquest.Assets.Entity.Stats.Template
{
	public class TemplateHouse: IEntityTemplate
	{
		public HealthStats healthStats {get; private set;}
		
		public TemplateHouse ()
		{
			healthStats = new HealthStats(1000);
		}

	}
}

