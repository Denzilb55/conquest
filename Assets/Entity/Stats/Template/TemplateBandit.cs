using System;

namespace Conquest.Assets.Entity.Stats.Template
{
	public class TemplateBandit : IEntityTemplate
	{
		public HealthStats healthStats {get; private set;}
		public EnergyStats energyStats {get; private set;}
		public AttackerStats attackerStats {get; private set;}
		
		public TemplateBandit ()
		{
			healthStats = new HealthStats(50);
			energyStats = new EnergyStats(18);
			attackerStats = new AttackerStats(6,3,1);
		}
	}
}

