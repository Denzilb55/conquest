using System;

namespace Conquest.Assets.Entity.Stats.Template
{
	public class TemplateSoldier: IEntityTemplate
	{
		public HealthStats healthStats {get; private set;}
		public EnergyStats energyStats {get; private set;}
		public AttackerStats attackerStats {get; private set;}
		
		public TemplateSoldier ()
		{
			healthStats = new HealthStats(40);
			energyStats = new EnergyStats(14);
			attackerStats = new AttackerStats(8,2,1);
		}

	}
}

