using System;
using System.Collections.Generic;
using Conquest.Assets.Teams;

namespace Conquest.Assets.Entity.Stats.Template
{
	public static class EntityTemplate
	{
		public static readonly TemplateHouse [] House = new TemplateHouse[Team.TEAM_COUNT];
		public static readonly TemplateSoldier [] Soldier = new TemplateSoldier[Team.TEAM_COUNT];
		public static readonly TemplateBandit [] Bandit = new TemplateBandit[Team.TEAM_COUNT];
		public static readonly TemplateBountyHunter [] BountyHunter = new TemplateBountyHunter[Team.TEAM_COUNT];
		
		static EntityTemplate()
		{
			for (int i = 0; i < Team.TEAM_COUNT; i++)
			{
				House[i] = new TemplateHouse();	
				Soldier[i] = new TemplateSoldier();
				Bandit[i] = new TemplateBandit();
				BountyHunter[i] = new TemplateBountyHunter();
				
			}
		}
	}
}

