using System;
using System.Collections.Generic;
using Conquest.Assets.Entity;
using Conquest.Assets.Teams;
using System.Linq;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Management
{
	public class EntityManager
	{

		private List<IEntity> [,] entities; 
		public const int AREA_SIZE = 32;
		
		
		public EntityManager ()
		{
			entities  = new List<IEntity>[World.WORLD_WIDTH/AREA_SIZE,World.WORLD_HEIGHT/AREA_SIZE];
			
			for (int i = 0; i < World.WORLD_WIDTH/AREA_SIZE; i++)
			{
				for (int j = 0; j < World.WORLD_HEIGHT/AREA_SIZE; j++)
				{
					entities[i,j] = new List<IEntity>();
				}
			}
		}
		
		private int MapToArea(int val)
		{
			return val / AREA_SIZE;
		}
		
		
		public void Add(IEntity entity)
		{
			entities[MapToArea(entity.GetWorldX()),MapToArea(entity.GetWorldY())].Add(entity);
			
		}
		
		public void Remove(IEntity entity)
		{
			entities[MapToArea(entity.GetWorldX()),MapToArea(entity.GetWorldY())].Remove(entity);	
		}
		
		public IEntity GetEntityAt (int worldX, int worldY)
		{
			foreach (IEntity e in entities[MapToArea(worldX),MapToArea(worldY)])
			{
				if (e.GetWorldX() == worldX && e.GetWorldY() == worldY)
				{
					return e;	
				}
			}
			
			return null;
		}
		
		//rewrite this to return all entities in an area
		public List<IEntity> GetEntitiesInArea (int areaX, int areaY)
		{
			return entities[areaX, areaY];
		}
		
		public List<IEntity> GetEntitiesAroundCoordinate (int worldX, int worldY)
		{
			return GetEntitiesAroundArea(MapToArea(worldX), MapToArea(worldY));
		}
		
		public List<IEntity> GetEntitiesAroundCoordinate(int worldX, int worldY, Team team)
		{
			List<IEntity> entityList = new List<IEntity>();
			
			int areaX = MapToArea(worldX);
			int areaY = MapToArea(worldY);
			
			int startX = areaX - 1;
			int startY = areaY - 1;
			
			int endX = areaX + 1;
			int endY = areaY + 1;
			
			if (startX < 0) startX = 0;
			if (startY < 0) startY = 0;
			if (endX > World.WORLD_WIDTH/AREA_SIZE -1) endX = World.WORLD_WIDTH/AREA_SIZE -1;
			if (endY > World.WORLD_HEIGHT/AREA_SIZE -1) endY = World.WORLD_HEIGHT/AREA_SIZE -1;
			
			if (startX < 0) startX = 0;
			if (startY < 0) startY = 0;
			if (endX > World.WORLD_WIDTH -1) endX = World.WORLD_WIDTH -1;
			if (endY > World.WORLD_HEIGHT -1) endY = World.WORLD_HEIGHT -1;
			
			for (int i = startX; i <= endX; i++)
			{
				for (int j = startY; j <= endY; j++)
				{
					//entityList.AddRange(entities[i,j]);
					entityList.AddRange(entities[ i,j].Where(o => o.IsOwnedBy(team)).ToList());
				}	
			}
			
			return entityList;
		}
		
		public List<IEntity> GetEntitiesAroundArea (int areaX, int areaY)
		{
			List<IEntity> entityList = new List<IEntity>();
			
			int startX = areaX - 1;
			int startY = areaY - 1;
			
			int endX = areaX + 1;
			int endY = areaY + 1;
			
			if (startX < 0) startX = 0;
			if (startY < 0) startY = 0;
			if (endX > World.WORLD_WIDTH/AREA_SIZE -1) endX = World.WORLD_WIDTH/AREA_SIZE -1;
			if (endY > World.WORLD_HEIGHT/AREA_SIZE -1) endY = World.WORLD_HEIGHT/AREA_SIZE -1;
			
			for (int i = startX; i <= endX; i++)
			{
				for (int j = startY; j <= endY; j++)
				{
					entityList.AddRange(entities[i,j]);	
				}	
			}
			
			return entityList;
		}
		
		public List<IEntity> GetEntitiesOfTeam(Team team)
		{
			List<IEntity> teamList = new List<IEntity>();
			
			foreach (List<IEntity> list in entities)
			{
				teamList.AddRange(list.Where(o => o.IsOwnedBy(team)).ToList());
			}
			
			return teamList;
		}
	}
}

