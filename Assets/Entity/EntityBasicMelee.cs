using System;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity.Stats.Template;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity
{
	public abstract class EntityBasicMelee  : EntityBasic, IStandardUnit
	{
		public MovementModule movementModule {get; protected set;}
		public AttackModule attackModule {get; protected set;}
		public HealthModule healthModule {get; protected set;}
		public DefensiveModule defenderModule {get; protected set;}
		public EnergyModule energyModule {get; protected set;}
		public BackpackModule backpackModule {get; protected set;}
		public ItemHandlingModule itemHandlerModule {get; protected set; }
		
		public EntityBasicMelee (int PosX, int PosY, Team owner, StanceType stance) : base(PosX, PosY, owner, stance)
		{
			itemHandlerModule = new ItemHandlingModule();
		}
		

		#region IMovable implementation
		public MoveNode FindMovableZone (World world)
		{
			return movementModule.FindMovableZone(world);
		}
	
		/*public void MoveTo (World world, int PosX, int PosY)
		{
			movementModule.MoveTo(world, PosX, PosY);
		}*/
		
		public MovementModule GetMoveModule()
		{
			return movementModule;	
		}
		
		public int GetPosX ()
		{
			return PosX;
		}
	
		public int GetPosY ()
		{
			return PosY;
		}
		
		public void SetPosX (int value)
		{
			PosX = value;
		}
	
		public void SetPosY (int value)
		{
			PosY = value;
		}
		#endregion

		#region IHealthy implementation
		public HealthModule GetHealthModule ()
		{
			return healthModule;
		}
		#endregion

		#region IAttacker implementation
		public AttackModule GetAttackModule ()
		{
			return attackModule;
		}
		#endregion

		#region IDefender implementation
		public DefensiveModule GetDefenderModule ()
		{
			return defenderModule;
		}
		#endregion


		#region IEnergetic implementation
		public EnergyModule GetEnergyModule ()
		{
			return energyModule;
		}
		#endregion


		#region IMovable implementation
		public EntityBasic GetEntity ()
		{
			return this;
		}
		#endregion


		#region IBackpacked implementation
		public void AddToBackpack (IItem item)
		{
			backpackModule.Add(item);
		}
		#endregion
	
	


		#region IItemWielder implementation
		public ItemHandlingModule GetItemHandlingModule ()
		{
			return itemHandlerModule;
		}
		#endregion


	}
}

