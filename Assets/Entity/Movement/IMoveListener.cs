using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Movement
{
	public interface IMoveListener
	{
		void NotifyMove(World world, IEntity movedEntity);
	}
}

