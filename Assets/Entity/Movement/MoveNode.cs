using System;
using Conquest.Assets.UtilityGeneral;
using System.Collections.Generic;

namespace Conquest.Assets.Entity.Movement
{
	public class MoveNode
	{
		private List<MoveNode> next = new List<MoveNode>(3);
		private MoveNode previous;
		public readonly IntVector coords;
		public int BranchCount 
		{
			get { return next.Count; }
		}
		public int NodeEnergy {get; private set;}
		
		public int X
		{
			get {return coords.X;}	
		}
		
		public int Y
		{
			get {return coords.Y;}	
		}
		
		public MoveNode (int x, int y, MoveNode previous, int nodeEnergy)
		{
			coords = new IntVector(x, y);
			this.previous = previous;
			NodeEnergy = nodeEnergy;
		}
		
		public void HookNext(MoveNode node)
		{
			next.Add(node);
		}
		
		public MoveNode GetChildNode(int index)
		{
			return next[index];
		}
		
		public MoveNode GetParent()
		{
			return previous;	
		}
		
		public MoveNode FindCoordsInChildren(int x, int y)
		{
			if (X == x && Y == y)
			{
				return this;	
			}
			else
			{
				foreach (MoveNode m in next)
				{
					MoveNode foundNode = m.FindCoordsInChildren(x,y);
					
					if (foundNode != null) return foundNode;
				}
			}
			
			return null;
		}
		
		public MoveNode FindClosestTo(int x, int y)
		{
			MoveNode closestNode = this;
			int dist = GetDistMan(x,y,closestNode.coords);
			
			if (dist == 1) return this;
			
			foreach (MoveNode m in next)
			{
				MoveNode foundNode = m.FindClosestTo(x,y);
				int nodeDist = GetDistMan(x,y,foundNode.coords);
				
				if (nodeDist < dist)
				{
					dist = nodeDist;
					closestNode = foundNode;
					
					/*if (dist == 1)
					{
						break;	
					}*/
				}
				else if (nodeDist == dist && foundNode.NodeEnergy > closestNode.NodeEnergy)
				{
					dist = nodeDist;
					closestNode = foundNode;
				}

			}
			
			return closestNode;
		}
		
		private int GetDistMan(int x, int y, IntVector coords)
		{
			
			return (int)(Math.Abs(x - coords.X) + Math.Abs(y - coords.Y));
		}
		
		public MoveNode TrimFromParent()
		{
			previous.RemoveChild(this);
			
			return this;
		}
		
		public void RemoveChild(MoveNode node)
		{
			next.Remove(node);	
		}
		
		public static LinkedList<MoveNode> ExtractPathFromMovementZone(MoveNode destinationNode)
		{
			LinkedList<MoveNode> movePath = new LinkedList<MoveNode>();
			
			while (destinationNode.GetParent() != null)
			{
				movePath.AddFirst(destinationNode);
				destinationNode = destinationNode.GetParent();	
			}
			
			return movePath;
		}
		
	}
}

