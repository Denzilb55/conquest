using System;

namespace Conquest.Assets.Entity.Movement
{
	public interface IMovementCallback
	{
		void MovementEnd();
	}
}

