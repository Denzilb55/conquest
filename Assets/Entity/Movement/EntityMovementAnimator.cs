using System;
using UnityEngine;
using System.Collections.Generic;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.Graphics;
using Conquest.Assets.GameInput;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Movement
{
	public class EntityMovementAnimator
	{
		private float deltaTime;
		public LinkedListNode<MoveNode> pathNode;
		private IEntity animationEntity
		{
			get
			{
				return movable;
			}
		}
		
		private EnergyModule energyModule
		{
			get
			{
				if (movable == null) return null;
				
				return movable.GetEnergyModule();
			}
		}
		
		private MovementModule moveModule
		{
			get
			{
				if (movable == null) return null;
				
				return movable.GetMoveModule();
			}
		}
		
		private InputHandler inputHandler;
		private IMovementCallback callback;
		
		private float deltaX = 0;
		private float deltaY = 0;
		private int i = 0;
		
		public float animationPeriod = 0.32f;
		
		private IEnergeticMovable movable;
		
		
		public EntityMovementAnimator (InputHandler inputHandler)
		{
			this.inputHandler = inputHandler;
		}


		public void StartMove(IEnergeticMovable energeticMovable, LinkedList<MoveNode> path, IMovementCallback callback)
		{
			pathNode = path.First;	
			movable = energeticMovable;
			deltaTime = 0;
			animationEntity.MakeInvisible();
			this.callback = callback;
			i = 0;
		}
				
		
		public void Step(World world)
		{
			if (animationEntity != null)
			{
				

				if (deltaTime / animationPeriod >= 1 || !world.explored[animationEntity.GetWorldX(), animationEntity.GetWorldY()])
				{
					energyModule.SetEnergy(pathNode.Value.NodeEnergy);	
					animationEntity.PlaceAt(world, pathNode.Value.X, pathNode.Value.Y);
					//UnityEngine.Debug.Log(i++ +": "+ pathNode.Value.X + " " + pathNode.Value.Y);
					deltaTime = 0;
					deltaX = 0;
					deltaY = 0;
					pathNode = pathNode.Next;
					
					if (pathNode == null) 
					{
						EndAnimation();	
					}
				}	
			}
		}
		
		public void Render(GameGraphics g, World world)
		{
			if (animationEntity != null && world.explored[animationEntity.GetWorldX(), animationEntity.GetWorldY()])
			{
				deltaTime += Time.fixedDeltaTime;
				
				deltaX = (pathNode.Value.X - animationEntity.GetWorldX()) *  deltaTime/animationPeriod;
				deltaY = (pathNode.Value.Y - animationEntity.GetWorldY()) * deltaTime /animationPeriod;
				
				float xFocus = (animationEntity.GetWorldX() + deltaX) * SpriteSheet.ICON_WIDTH;
				float yFocus = (animationEntity.GetWorldY() + deltaY) * SpriteSheet.ICON_HEIGHT;
				
				world.FocusViewport(xFocus, yFocus);
				g.RenderTileToWorldCoords(world, animationEntity.GetSprite().index, animationEntity.GetWorldX() + deltaX, animationEntity.GetWorldY() + deltaY, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
			}
		}
		
		public void EndAnimation()
		{
			animationEntity.MakeVisible();
			movable = null;
			inputHandler.EndMovement();
			
			if (callback != null)
			{
				callback.MovementEnd();
			}
		}
		
		public bool IsAnimating()
		{
			return animationEntity != null;	
		}
	}
	
}

