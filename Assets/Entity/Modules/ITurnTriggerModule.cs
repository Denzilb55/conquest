using System;

namespace Conquest.Assets.Entity.Modules
{
	public interface ITurnTriggerModule
	{
		void StartTurn();
	}
}

