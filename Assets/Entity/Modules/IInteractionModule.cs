using System;
using Conquest.Assets.GameInput;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Modules
{
	public interface IInteractionModule
	{
		bool DoModuleCommandAction (Conquest.World world, Conquest.Assets.GameInput.IActionCallback callback, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity);
		string GetInteractiveText ();
		bool CanInteract(World world, IEntity entity);
	}
}

