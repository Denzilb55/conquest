using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Tiles;
using Conquest.Assets.GameInput;
using Conquest.Assets.Teams;

namespace Conquest.Assets.Entity.Modules
{
	public class AttackModule: IInteractionModule
	{
		protected AttackerStats attackerStats {private get; set;}
		public readonly IStandardUnit unit;
		
		/*public IEntity entity
		{
			get { return unit; }	
		}*/
		
		public AttackModule (IStandardUnit unit, AttackerStats attackerStats)
		{
			this.unit = unit;
			this.attackerStats = attackerStats;
		}

		#region IModule implementation
		/*public void DoModuleSelectAction (World world, Conquest.Assets.GameInput.IActionCallback callback)
		{
			
		}*/
	
		//needs refactor for defence and counter attack
		public bool DoModuleCommandAction (Conquest.World world, IActionCallback callback, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity)
		{
			if (unit.IsDestroyed()) return false;
			bool attacked = false;
			
			if (unit.GetEnergyModule().GetEnergy() < attackerStats.EnergyCost) return false;
			
			IHealthy healthy = entity as IHealthy;
			
			if (healthy != null) //if target is type healthy deal damage
			{

				if (!unit.GetOwner().TryReduceActivePoints(1)) return false;
				
				attacked = StrikeTarget (world, callback, entity, healthy);
			}
			else{
				return false;	
			}
			
			if (callback != null)
			{
				callback.Attack();
			}
			
			return attacked;
		}
		#endregion
		
		public bool CanInteract (World world, IEntity entity)
		{
			return (entity != null && entity != unit && entity.GetOwner() != unit.GetOwner() && IsInStrikeRange(entity.GetWorldX(), entity.GetWorldY()));
		}
		
		public bool HasSufficientEnergy()
		{
			return unit.GetEnergyModule().energyStats.Energy >= attackerStats.EnergyCost;
		}
		
		public bool HasSufficientActivePoints()
		{
			return unit.GetAttackModule().attackerStats.ActivePointCost <= unit.GetOwner().ActivePoints;
		}

		private bool StrikeTarget (World world, IActionCallback callback, IEntity target, IHealthy healthy)
		{
			bool attacked = false;
			
			unit.GetEnergyModule().ReduceEnergy(attackerStats.EnergyCost);
			int attackerDamage = GetDamage() - Tile.GetDefensiveScore(target.GetCurrentTile(world).ID, (int)target.GetEntityType());
			
			if (attackerDamage < 0) attackerDamage = 0;
			
			healthy.GetHealthModule().DamageHealth(attackerDamage, world, unit);
			attacked = true;
			target.SetStance(StanceType.NeutralAggressive);
			
			if (!target.IsDestroyed())
			{
				ICounterAttacker counterer = healthy as ICounterAttacker;
				
				if (counterer != null) //if target is of type counter attacker attempt to counter
				{
					IHealthy entityHealthy = unit as IHealthy;
					
					if (entityHealthy != null) //if first entity is of type healthy get damaged by counterer
					{
						int countererDamage = counterer.GetAttackModule().GetDamage() - Tile.GetDefensiveScore(unit.GetCurrentTile(world).ID,(int)unit.GetEntityType());
						
						if (countererDamage < 0) countererDamage = 0;
						
						entityHealthy.GetHealthModule().DamageHealth(countererDamage, world, target);
						
						if (unit.IsDestroyed())
						{
							if (callback != null)
							{
								callback.CloseInteractMenu();
							}
						}
					}
				}
			}
			else
			{
				if (callback != null)
				{
					callback.CloseInteractMenu();
				}
			}

			
			return attacked;
		} 
		
		public List<IEntity> GetInRangeEntities(World world)
		{
			int range = 1;
			
			List<IEntity> inRangeEntities = new List<IEntity>();
			List<IEntity> nearbyEntities = world.entityManager.GetEntitiesAroundCoordinate(unit.GetWorldX(), unit.GetWorldY());
			
			foreach (IEntity e in nearbyEntities)
			{
				if (e.GetOwner() == unit.GetOwner())
				{
					continue;	
				}
				
				if (Math.Abs(e.GetWorldX() - unit.GetWorldX()) + Math.Abs(e.GetWorldY() - unit.GetWorldY())	 <= range)
				{
					inRangeEntities.Add(e);	
				}
			}
			
			return inRangeEntities;
		}
		
		public bool IsInStrikeRange(int worldX, int worldY)
		{
			//UnityEngine.Debug.Log(worldX + " : " + entity.GetWorldX() + " -- " + worldY + " : " + entity.GetWorldY());
			return IsDistanceInRange(unit.GetManhattenDistanceTo(worldX, worldY));
			
			//return (Math.Abs(worldX - unit.GetWorldX()) == 1 && worldY == unit.GetWorldY()) || (Math.Abs(worldY - unit.GetWorldY()) == 1 && worldX == unit.GetWorldX());
		}
		
		public bool IsDistanceInRange(int distance)
		{
			return distance <= 1;
		}

		#region ICombatStats implementation
		public int GetDamage ()
		{
			return attackerStats.Damage;
		}
		#endregion

		public void ModifyDamage(int value)
		{
			attackerStats = new AttackerStats(attackerStats.Damage + value, attackerStats.EnergyCost, attackerStats.ActivePointCost);	
		}

		#region IInteractiveModule implementation
		public string GetInteractiveText ()
		{
			return "Attack";
		}
		#endregion


	}
}

