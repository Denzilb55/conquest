using System;
using Conquest.Assets.Entity.Stats;

namespace Conquest.Assets.Entity.Modules
{
	public class EnergyModule: ITurnTriggerModule
	{
		public EnergyStats energyStats;
		public readonly EntityBasic entity;
		
		public EnergyModule (EntityBasic entity, EnergyStats energyStats)
		{
			this.energyStats = energyStats;
			this.entity = entity;
		}
		
		#region IMovementStats implementation
		public int GetEnergy ()
		{
			return energyStats.Energy;
		}
	
		public int GetMaxEnergy ()
		{
			return energyStats.MaxEnergy;
		}
		
		#endregion
		
		
		public void SetEnergy(int value)
		{
			energyStats = new EnergyStats(value, energyStats.MaxEnergy);
		}
		
		public void ReduceEnergy(int value)
		{
			energyStats = new EnergyStats(energyStats.Energy - value, energyStats.MaxEnergy);	
		}

		#region ITurnTriggerModule implementation
		public void StartTurn ()
		{
			SetEnergy(GetMaxEnergy());
		}
		#endregion
	}
}

