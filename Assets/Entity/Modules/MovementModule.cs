using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.GameInput;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity;
using Conquest.Assets.Tiles;
using Conquest.Assets.Entity.Modules.Interfaces;


namespace Conquest.Assets.Entity.Modules
{
	public class MovementModule: IActiveModule
	{
		public readonly IEnergeticMovable movable;
		
		public int PosX
		{
			get { return movable.GetWorldX(); }
			set { movable.SetPosX(value); }
		}
		
		public int PosY
		{
			get { return movable.GetWorldY(); }
			set { movable.SetPosY(value); }
		}
		
		public MovementModule (IEnergeticMovable movable)
		{
			this.movable = movable;
		}
		
		/*public void MoveTo (World world, int PosX, int PosY)
		{
			world.entityManager.Remove(movable);
			this.PosX = PosX;
			this.PosY = PosY;
			world.entityManager.Add(movable);			
		}*/
		
		/// <summary>
		/// Finds the movable zone, in which an entity can move with its given energy.
		/// </summary>
		/// <returns>
		/// The movable zone.
		/// </returns>
		/// <param name='world'>
		/// World.
		/// </param>
		public MoveNode FindMovableZone(World world)
		{
			MoveNode stem = new MoveNode(PosX, PosY, null, movable.GetEnergyModule().GetEnergy());
			
			LinkedList<MoveNode> outerNodes = new LinkedList<MoveNode>();
			outerNodes.AddFirst(stem);
			SpreadMovableZone(world, ref outerNodes, stem);
			
			return stem;
		}
		
		/*
		 * Spread out current movable zone. Will only happen if energy is available
		 */
		private void SpreadMovableZone(World world, ref LinkedList<MoveNode> outerNodes, MoveNode stem)
		{
			while (outerNodes.Count != 0) //iterate through all outer nodes and spread
			{
				LinkedList<MoveNode> newOuterNodes = new LinkedList<MoveNode>(); // clear list of outer nodes
				
				foreach (MoveNode node in outerNodes)
				{
					SpreadMoveNode(world, node, ref newOuterNodes, stem);
				}
				
				outerNodes = newOuterNodes; //update set of outer nodes
			}

		}
		
		//Spread one movement node in the current zone in all possible directions
		private void SpreadMoveNode (World world, MoveNode node, ref LinkedList<MoveNode> outerNodes, MoveNode stem)
		{
			if (node.X -1 >= 0 && (node.GetParent() == null || node.GetParent().coords.X != node.X -1))
			{
				SpreadMoveNodeTo(world, node.X-1, node.Y, node, ref outerNodes, stem);
			}
			
			if (node.X +1 < World.WORLD_WIDTH && (node.GetParent() == null || node.GetParent().coords.X != node.X +1))
			{
				SpreadMoveNodeTo(world, node.X+1, node.Y, node, ref outerNodes, stem);
			}
			
			if (node.Y -1 >= 0 && (node.GetParent() == null || node.GetParent().coords.Y != node.Y -1))
			{
				SpreadMoveNodeTo(world, node.X, node.Y-1, node, ref outerNodes, stem);
			}
			
			if (node.Y +1 < World.WORLD_HEIGHT && (node.GetParent() == null || node.GetParent().coords.Y != node.Y +1))
			{
				SpreadMoveNodeTo(world, node.X, node.Y+1, node, ref outerNodes, stem);
			}
		}
		
		//spread one node to a particular coordinate (should be adjacent to make sense)
		private void SpreadMoveNodeTo(World world, int x, int y, MoveNode parentNode, ref LinkedList<MoveNode> outerNodes, MoveNode stem)
		{
			int energyCost = Tile.GetMovementScore(world.GetTile(x,y).ID, (int)movable.GetEntityType());
			int newEnergy = parentNode.NodeEnergy - energyCost;
			bool slowTile = (energyCost == 0);
			bool impassible = (energyCost == -1);
			
			if (impassible) return;
			
			if (newEnergy > 0 ||  (slowTile && newEnergy != 0))
			{
				if (slowTile)
				{
					newEnergy = 0;	
				}
				
				IEntity collidedEntity = world.entityManager.GetEntityAt(x, y);
				
				if (collidedEntity != null)
				{
					return;	
				}				
					
				MoveNode existingNode = stem.FindCoordsInChildren(x, y);
				
				if (existingNode != null)
				{
			
					if (existingNode.NodeEnergy > newEnergy)
					{
						return; //do not add node if better path already leads to this coordinate
					}
					else
					{
						existingNode.TrimFromParent(); //if path already leads to this coordinate, but this one is better, remove old path
						outerNodes.Remove(existingNode);
					}
				}
				
				MoveNode newNode = new MoveNode(x, y, parentNode, newEnergy);
				parentNode.HookNext(newNode); // add node to tree
				if (energyCost != 0)
				{
					outerNodes.AddLast(newNode); //add node to list defining it as outer node
				}
			}

		}

		/*public override void DoSelectAction (World world, IActionCallback callback)
		{
			callback.SetMoveZone(this, world);
			base.DoSelectAction (world, callback);
		}
		
		public override void ExecuteInputCommand(World world, IActionCallback callback, int commandX, int commandY)
		{
			callback.MoveAndAnimate(this, commandX, commandY);
			base.ExecuteInputCommand(world, callback, commandX, commandY);
		}*/

		#region IModule implementation
		public void DoModuleSelectAction (World world, IActionCallback callback)
		{
			callback.SetMoveZone(movable);
		}
	
		public void DoModuleCommandAction (World world, IActionCallback callback, int commandX, int commandY)
		{
			if (PosX != commandX || PosY != commandY)
			{
				int playerPointCost = 1;

				if (!movable.GetOwner().TryReduceActivePoints(playerPointCost)) return;
				
				if (!callback.MoveAndAnimate(movable, commandX, commandY))
				{
					movable.GetOwner().RewardPlayerPoints(playerPointCost);
				}
			}
			
		}
		
		public bool CanInteract (World world, int commandX, int commandY)
		{
			return true;	
		}
		#endregion
	}
}

