using System;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.GameInput;

namespace Conquest.Assets.Entity.Modules
{
	public class InteractionModule: IInteractionModule
	{
		private IEntity entity;
		
		public InteractionModule (IEntity entity)
		{
			this.entity = entity;
		}
		
		public bool CanInteract(World world, IEntity entity)
		{

			ICommunicator communicator = entity as ICommunicator;
			if (communicator != null && communicator.CanCommunicate())
			{
				return communicator.GetCommunicationModule().CanInteract(world, this.entity);
			}
			return false;
		}
		
		public bool DoModuleCommandAction (Conquest.World world, Conquest.Assets.GameInput.IActionCallback callback, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity)
		{
			bool communicated = false;
			
			ICommunicator communicator = entity as ICommunicator;
			
			if (communicator != null && communicator.CanCommunicate())
			{	
				communicator.GetCommunicationModule().DoModuleCommandAction(world, callback, this.entity);
				communicated = true;
			}
			
			callback.CloseInteractMenu();
			
			return communicated;
		}
	
		public string GetInteractiveText ()
		{
			return "Communicate";
		}
	}
}

