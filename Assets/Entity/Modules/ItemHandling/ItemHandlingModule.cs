using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity.Modules
{
	public class ItemHandlingModule
	{
		public List<IItemSlotModule> itemSlots = new List<IItemSlotModule>();
		
		
		public ItemHandlingModule ()
		{
	
		}
		
		public void Add(IItemSlotModule slot)
		{
			itemSlots.Add(slot);	
		}
		
		public bool Equip(IItem item)
		{
			foreach (IItemSlotModule slot in itemSlots)
			{
				if (slot.Equip(item))
				{
					return true;
				}
			}
			return false;
		}
		
	
	}
}

