using System;
using Conquest.Assets.Entity.Items;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Modules
{
	public class ItemSlotModule <I>: IItemSlotModule where I: IItem
	{
		private IStandardUnit unit;
		public I Item;
		
		public String SlotLabel
		{
			private set;
			get;
		}
		
		public ItemSlotModule (IStandardUnit standardUnit, String slotLabel)
		{
			unit = standardUnit;
			SlotLabel = slotLabel;
		}
		
		public bool Equip(IItem item)
		{
			if (Item == null)
			{
				Item = (I)item;
				item.Equip(unit);
				return true;
			}
			
			return false;
		}
		
		
		public I Unequip()
		{
			I item = Item;
			Item = default(I);
			if (Item != null) UnityEngine.Debug.Log("NOT NULL");
			return item;
		}
		
		public Type GetItemType()
		{
			return typeof(I);	
		}
		
		public override string ToString ()
		{
			return SlotLabel;
		}
		
		public String GetItemLabel()
		{
			if (Item == null)
			{
				return "Empty";	
			}
			
			return Item.GetName();
		}
	}
	
}

