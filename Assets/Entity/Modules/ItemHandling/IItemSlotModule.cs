using System;
using Conquest.Assets.Entity.Items;
using Conquest.Assets.Entity.Modules;

namespace Conquest.Assets.Entity.Modules
{
	public interface IItemSlotModule 
	{
		bool Equip(IItem item);
		Type GetItemType();
		String GetItemLabel();
	}
}

