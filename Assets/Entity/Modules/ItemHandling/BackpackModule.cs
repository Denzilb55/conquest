using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity.Modules
{
	public class BackpackModule
	{
		private List<IItem> backpack = new List<IItem>();
		private int storageSpace;
		
		public BackpackModule (int storageSpace)
		{
			this.storageSpace = storageSpace;
		}
		
		public bool Add(IItem item)
		{
			if (backpack.Count < storageSpace)
			{
				backpack.Add(item);
				return true;
			}
			
			return false;
		}
	}
}

