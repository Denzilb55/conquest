using System;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.GameInput;

namespace Conquest.Assets.Entity.Modules
{
	public interface ICommunicatorModule: IModule
	{
		void DoModuleCommandAction (Conquest.World world, Conquest.Assets.GameInput.IActionCallback callback, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity);
		bool CanInteract (Conquest.World world, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity);
	}
}

