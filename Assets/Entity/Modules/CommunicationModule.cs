using System;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Modules.CommunicationResponse;
using Conquest.Assets.Quests;
using Conquest.Assets.Quests.Rewards;

namespace Conquest.Assets.Entity.Modules
{
	public class CommunicationModule: ITriggerModule, ICommunicatorModule
	{
		private IEntity entity;
		private ICommunicationResponse CResponse { get; set; }
		
		public CommunicationModule (IEntity entity, World world)
		{
			this.entity = entity;
			
			int rand = UnityEngine.Random.Range(1, 10);	
			IReward reward = null;
			
			if (UnityEngine.Random.Range(0, 3) == 0)
			{
				reward = new RewardHealthBoost((int)Math.Pow(rand,1.5));
			}
			else
			{
				reward = new RewardHealAllUnits(rand*3);
			}
			
			CResponse = new CResponseQuestOffer(new QuestKillEntityType( world, rand, EntityType.Bandit, entity.GetOwner(), reward));	
		}

		#region IInteractiveModule implementation
		public bool CanInteract (Conquest.World world, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity)
		{
			if (CResponse == null) return false;
			return (Math.Abs(this.entity.GetWorldX() - entity.GetWorldX()) == 1 && this.entity.GetWorldY() == entity.GetWorldY()) || (Math.Abs(this.entity.GetWorldY() - entity.GetWorldY()) == 1 && this.entity.GetWorldX() == entity.GetWorldX());
		}
	
		#endregion


		#region ICommunicatorModule implementation
		public void DoModuleCommandAction (Conquest.World world, Conquest.Assets.GameInput.IActionCallback callback, Conquest.Assets.Entity.Modules.Interfaces.IEntity entity)
		{
			if (CResponse != null)
			{
				CResponse.PromptCommunicationResponse(world);
				CResponse = null;
			}
		}
		#endregion


	}
}

