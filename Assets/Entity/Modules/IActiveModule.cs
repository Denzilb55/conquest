using System;
using Conquest.Assets.GameInput;

namespace Conquest.Assets.Entity.Modules
{
	public interface IActiveModule : IModule
	{
		void DoModuleSelectAction(World world, IActionCallback callback);
		void DoModuleCommandAction(World world, IActionCallback callback, int commandX, int commandY);
	}
}

