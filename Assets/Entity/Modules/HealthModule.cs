using System;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Modules
{
	public class HealthModule: IModule, ITurnTriggerModule
	{
		public HealthStats healthStats {get; protected set;}
		public IEntity entity { get; private set; }
		
		public HealthModule (IEntity entity, HealthStats healthStats)
		{
			this.healthStats = healthStats;
			this.entity = entity;
		}
		
		public void DamageHealth(int value, World world, IEntity damager)
		{
			if (value < 0) Heal(-value);
			
			healthStats = new HealthStats(healthStats.Health - value, healthStats.MaxHealth);
			
			if (healthStats.Health <= 0)
			{
				entity.Destroy(world);
				world.NotifyDestroy(entity, damager);
			}
		}
		
		public void Heal(int value)
		{
			if (value < 0) return;
			
			int newHealth = value + healthStats.Health;
			
			if (newHealth > healthStats.MaxHealth) newHealth = healthStats.MaxHealth;
			
			healthStats = new HealthStats(newHealth, healthStats.MaxHealth);
		}
		
		public void HealFull()
		{
			healthStats = new HealthStats(healthStats.MaxHealth, healthStats.MaxHealth);
		}
		
		public void IncreaseMaxHealth(int amount)
		{
			healthStats = new HealthStats(healthStats.Health + amount, healthStats.MaxHealth + amount);	
		}
				
		#region ITurnTriggerModule implementation
		public void StartTurn ()
		{
			
		}
		#endregion
	}
}

