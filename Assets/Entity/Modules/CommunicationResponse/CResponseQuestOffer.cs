using System;
using System.Collections.Generic;
using Conquest.Assets.Quests.Rewards;
using Conquest.Assets.Quests;
using Conquest.Assets.Graphics;
using Conquest.Assets.Graphics.UtilityGUI;
using UnityEngine;
using Conquest.Assets.Teams;

namespace Conquest.Assets.Entity.Modules.CommunicationResponse
{
	public class CResponseQuestOffer: ICommunicationResponse
	{
		public QuestBasic Quest { get; private set; }
		private World world;
		
		public CResponseQuestOffer (QuestBasic quest)
		{
			Quest = quest;
		}

		#region ICommunicationResponse implementation
		public void PromptCommunicationResponse (World world)
		{
			string [] message = new string[4];
			message[0] = "Quest Offer: " + Quest.GetQuestTitle();
			message[1] = "I have a quest available! Here are the details:";
			message[2] = Quest.GetQuestDescription();
			message[3] = Quest.GetQuestRewardDescription();
				
			int [] spacing = new int[4];
			spacing[0] = 10;
			spacing[1] = 15;
			spacing[2] = 15;
			spacing[3] = 15;
			
			GUIStyle [] styles = new GUIStyle[4];
			styles[0] = GUIManager.titleStyle;
			styles[1] = GUIManager.subtitleStyle;
			styles[2] = GUIManager.reportStyle;
			styles[3] = GUIManager.reportStyle;
			
			List<ActionButton> buttons = new List<ActionButton>();
			
			Action action = () => 
			{
				Team.mainPlayer.AcceptQuest(world, Quest);
				GameGUI.guiManager.CloseSpecialReport();	
			};
			
			ActionButton button = new ActionButton(action, "Accept");
			buttons.Add(button);
			buttons.Add(ActionButton.ButtonCloseSpecialMessage);
			
			GameGUI.guiManager.SetSpecialMessage(message, Mood.Default, spacing, styles, buttons);
		}
		#endregion
	}
}

