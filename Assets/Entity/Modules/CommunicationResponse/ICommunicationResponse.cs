using System;

namespace Conquest.Assets.Entity.Modules.CommunicationResponse
{
	public interface ICommunicationResponse
	{
		void PromptCommunicationResponse(World world);
	}
}

