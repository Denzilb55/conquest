using System;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IDefender
	{
		DefensiveModule GetDefenderModule();
	}
}

