using System;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IBackpacked
	{
		void AddToBackpack(IItem item);
	}
}

