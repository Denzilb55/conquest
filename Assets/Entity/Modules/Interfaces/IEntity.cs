using System;
using System.Collections.Generic;
using Conquest.Assets.Graphics;
using Conquest.Assets.Graphics.UtilityGUI;
using Conquest.Assets.GameInput;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity;
using UnityEngine;
using Conquest.Assets.UtilityGeneral;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IEntity
	{
		EntityType GetEntityType();
		int GetWorldX();
		int GetWorldY();
		bool IsOwnedBy(Team player);
		Team GetOwner();
		Tiles.Tile GetCurrentTile(World world);
		bool IsDestroyed();
		void Destroy(World world);
		void MakeInvisible();
		void MakeVisible();
		Sprite GetSprite();
		void DoSelectAction(World world, IActionCallback callback);
		void DoInputCommand(World world, IActionCallback callback, int commandX, int commandY);
		bool CanInteract(World world, IActionCallback callback, IEntity entity, out List<ActionButton> buttons);
		bool IsInViewport(Rectangle viewport);
		void Render(GameGraphics g, int xScroll, int yScroll);
		void StartTurn(World world);
		void PlaceAt(World world, int worldX, int worldY);
		int GetManhattenDistanceTo(int x, int y);
		StanceType GetStance();
		void SetStance(StanceType stance);
	}
}

