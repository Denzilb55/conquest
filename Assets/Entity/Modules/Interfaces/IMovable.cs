using System;
using Conquest.Assets.Entity.Movement;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IMovable: IEntity
	{
		MoveNode FindMovableZone(World world);
		//void MoveTo (World world, int PosX, int PosY);
		MovementModule GetMoveModule();

		void SetPosX(int value);
		void SetPosY(int value);		
	}
}

