using System;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IEnergeticMovable: IMovable, IEnergetic
	{
	}
}

