using System;
using Conquest.Assets.Entity.Stats;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IEnergetic
	{
		EnergyModule GetEnergyModule();
	}
}

