using System;

//standard unit with attacking, energy, moving

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IStandardUnit: IEntity, IEnergeticMovable, IAttacker, ICounterAttacker, IHealthy, IDefender, IBackpacked, IItemWielder
	{
	}
}

