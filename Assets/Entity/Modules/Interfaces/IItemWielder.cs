using System;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IItemWielder
	{
		ItemHandlingModule GetItemHandlingModule();
	}
}

