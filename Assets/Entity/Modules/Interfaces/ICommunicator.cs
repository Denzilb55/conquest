using System;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface ICommunicator
	{
		bool CanCommunicate();
		CommunicationModule GetCommunicationModule();
	}
}

