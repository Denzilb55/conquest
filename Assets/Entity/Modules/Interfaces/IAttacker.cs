using System;
using Conquest.Assets.Entity.Modules;

namespace Conquest.Assets.Entity.Modules.Interfaces
{
	public interface IAttacker: IEntity
	{
		AttackModule GetAttackModule();
	}
}

