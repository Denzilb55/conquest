using System;
using Conquest.Assets.Graphics;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity.Stats.Template;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity
{
	public class EntitySoldier : EntityBasicMelee
	{

		public EntitySoldier (int PosX, int PosY, Team owner, StanceType stance) : base(PosX, PosY, owner, stance)
		{
			movementModule = new MovementModule(this);
			activeModules.Add(movementModule);
			
			attackModule = new AttackModule(this, EntityTemplate.Soldier[Owner].attackerStats);
			interactiveModules.Add(attackModule);
			
			healthModule = new HealthModule(this, EntityTemplate.Soldier[Owner].healthStats);
			turnTriggerModules.Add(healthModule);
			
			defenderModule = new DefensiveModule(this);
			energyModule = new EnergyModule(this, EntityTemplate.Soldier[Owner].energyStats);
			turnTriggerModules.Add(energyModule);
			
			ItemSlotModule<ISword> swordModule = new ItemSlotModule<ISword>(this, "Sword-Arm");
			itemHandlerModule.Add(swordModule);
			itemHandlerModule.Equip(Item.SwordStandard);
		}
		
		public override Sprite GetSprite ()
		{
			return Sprite.Soldier[Owner];
		}

		#region implemented abstract members of Conquest.Assets.Entity.EntityBasic
		public override EntityType GetEntityType ()
		{
			return EntityType.Soldier;
		}
		
		public override IEntityTemplate GetEntityTemplate ()
		{
			return EntityTemplate.Soldier[Owner];
		}
		#endregion

	}
}

