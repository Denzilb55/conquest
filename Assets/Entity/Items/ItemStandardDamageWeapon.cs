using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public abstract class ItemStandardDamageWeapon: ItemEquippable
	{
		private int damage;
		public string Name {get; private set;}
		
		public ItemStandardDamageWeapon (int damage, string name)
		{
			this.damage = damage;
			Name = name;
		}

		#region IItem implementation
		public override void Equip (IStandardUnit unit)
		{
			unit.GetAttackModule().ModifyDamage(damage);
		}
		
		public override void Unequip (IStandardUnit unit)
		{
			unit.GetAttackModule().ModifyDamage(-damage);
		}
		#endregion
		
		public override string GetName ()
		{
			return Name;
		}

	}
}

