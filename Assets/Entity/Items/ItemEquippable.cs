using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public abstract class ItemEquippable: IItem
	{
		public ItemEquippable ()
		{
		}
		
		#region IItem implementation
		public abstract void Equip (IStandardUnit unit);
		public abstract void Unequip (IStandardUnit unit);
		public abstract string GetName ();
		#endregion

	}
}

