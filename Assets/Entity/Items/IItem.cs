using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public interface IItem
	{
		void Equip(IStandardUnit unit);
		void Unequip(IStandardUnit unit);
		string GetName();
	}
}

