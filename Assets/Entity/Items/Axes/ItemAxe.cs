using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public class ItemAxe: ItemStandardDamageWeapon, IAxe
	{

		public ItemAxe (int damage, string name): base(damage, name)
		{
		}
		
		public override string GetName ()
		{
			return "Standard Axe";	
		}
		
	}
}

