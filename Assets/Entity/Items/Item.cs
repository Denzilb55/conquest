using System;
using System.Collections.Generic;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public static class Item
	{
		public static readonly ItemSword SwordStandard = new ItemSword(2, "Standard Sword");
		public static readonly ItemAxe AxeStandard = new ItemAxe(1, "Standard Axe");
		public static readonly ItemReaver AxeReaver = new ItemReaver();
		public static readonly ItemSword LongSword = new ItemSword(5, "Long, Pointy Sword");
	}
}

