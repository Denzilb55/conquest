using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public interface IConsumable: IItem
	{
		void Consume(IStandardUnit unit);
	}
}

