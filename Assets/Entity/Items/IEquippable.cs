using System;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity.Items
{
	public interface IWeapon
	{
		void Equip(IAttacker attacker);
		void Unequip(IAttacker attacker);
	}
}

