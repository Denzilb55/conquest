using System;
using Conquest.Assets.Teams;
using Conquest.Assets.Graphics;
using Conquest.Assets.Entity.Stats.Template;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Items;

namespace Conquest.Assets.Entity
{
	public class EntityBandit : EntityBasicMelee
	{
		public EntityBandit (int PosX, int PosY, Team owner, StanceType stance) : base(PosX, PosY, owner, stance)
		{
			movementModule = new MovementModule(this);
			activeModules.Add(movementModule);
			
			attackModule = new AttackModule(this, EntityTemplate.Bandit[Owner].attackerStats);
			interactiveModules.Add(attackModule);
			
			healthModule = new HealthModule(this, EntityTemplate.Bandit[Owner].healthStats);
			turnTriggerModules.Add(healthModule);
			
			defenderModule = new DefensiveModule(this);
			energyModule = new EnergyModule(this, EntityTemplate.Bandit[Owner].energyStats);
			turnTriggerModules.Add(energyModule);
			
			ItemSlotModule<IAxe> axeModule = new ItemSlotModule<IAxe>(this, "Mighty Axe-Hand");
			itemHandlerModule.Add(axeModule);
		//	itemHandlerModule.Equip(Item.AxeStandard);
			
		}

		public override IEntityTemplate GetEntityTemplate ()
		{
			return EntityTemplate.Bandit[Owner];
		}

		public override EntityType GetEntityType ()
		{
			return EntityType.Bandit;
		}

		public override Conquest.Assets.Graphics.Sprite GetSprite ()
		{
			return Sprite.Bandit[Owner];
		}

	}
}

