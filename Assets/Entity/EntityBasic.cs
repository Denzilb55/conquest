using System;
using System.Collections.Generic;
using Conquest.Assets.UtilityGeneral;
using Conquest.Assets.Graphics;
using Conquest.Assets.Graphics.UtilityGUI;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Entity.Stats.Template;
using Conquest.Assets.GameInput;
using Conquest.Assets.Entity.Modules;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Entity
{
	public enum EntityType {Soldier = 0, Bandit, BountyHunter}
	public enum StanceType {Main = 0, Opponent, NeutralPassive, NeutralAggressive}
	
	public abstract class EntityBasic: IEntity
	{
		//Positions based on world grid
		public int PosX {get; set;}
		public int PosY {get; set;}
		public bool Visible {get; protected set;}
		public Team Owner {get; private set;}
		private bool destroyed;
		public int EntityTypeID
		{
			get {return (int)GetEntityType(); }	
		}
		
		protected List<IActiveModule> activeModules = new List<IActiveModule>();
		protected List<ITurnTriggerModule> turnTriggerModules = new List<ITurnTriggerModule>();
		protected List<ITriggerModule> triggerModules = new List<ITriggerModule>();
		protected List<IInteractionModule> interactiveModules = new List<IInteractionModule>();
		
		protected InteractionModule intercomModule;
		
		public StanceType Stance { get; protected set; }
		
		//Positions based on world, per pixel
		public int PosPixelX
		{
			get
			{
				return PosX << SpriteSheet.ICON_SHIFT_WIDTH;	
			}
		}
		
		public int PosPixelY
		{
			get
			{
				return PosY << SpriteSheet.ICON_SHIFT_HEIGHT;	
			}
		}
		
		public Sprite SpriteImage
		{
			get {return GetSprite();}	
		}
		
		public EntityBasic (int PosX, int PosY, Team owner, StanceType stance)
		{
			this.PosX = PosX;
			this.PosY = PosY;
			Visible = true;
			this.Owner = owner;
			
			intercomModule = new InteractionModule(this);
			interactiveModules.Add(intercomModule);
			Stance = stance;
		}
		
		public void Render(GameGraphics g, int xScroll, int yScroll)
		{
			if (!Visible) return;
			g.RenderAtlasTile(SpriteImage.index, PosPixelX - xScroll, PosPixelY - yScroll, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
		}
		
		public bool IsInViewport(Rectangle viewport)
		{
			if (PosPixelX + SpriteSheet.ICON_WIDTH >= viewport.PosX && PosPixelX <= viewport.PosX + viewport.Width)
			{
				if (PosPixelY + SpriteSheet.ICON_HEIGHT >= viewport.PosY && PosPixelY <= viewport.PosY + viewport.Height)
				{
					return true;
				}
			}
				
			return false;
		}
		
		public void DoSelectAction(World world, IActionCallback callback)
		{
			foreach (IActiveModule module in activeModules)
			{
				module.DoModuleSelectAction(world, callback);	
			}
		}
		
		public void DoInputCommand(World world, IActionCallback callback, int commandX, int commandY)
		{
			foreach (IActiveModule module in activeModules)
			{
				module.DoModuleCommandAction(world, callback, commandX, commandY);
			}
		}
		
		public bool CanInteract(World world, IActionCallback callback, IEntity entity, out List<ActionButton> actionButons)
		{
			actionButons = new List<ActionButton>();
			
			
			foreach (IInteractionModule module in interactiveModules)
			{
				IInteractionModule mod = module; //delegate thing (reference memory) This line is NEEDED!!!
				
				if (mod.CanInteract(world, entity))
				{				
					System.Action action = new System.Action(() => mod.DoModuleCommandAction(world, callback, entity));
						
					ActionButton button = new ActionButton(action, mod.GetInteractiveText());
					actionButons.Add(button);
	
				}
			}
			
			
			if (actionButons.Count == 0) return false;
			
			return true;
		}
		
		public virtual bool IsSolid() //entity is collidable
		{
			return true;	
		}
		
		public void MakeInvisible()
		{
			Visible = false;	
		}
		
		public void MakeVisible()
		{
			Visible = true;	
		}
		
		 
		public abstract Sprite GetSprite();
		
		
		public void Destroy(World world)
		{
			world.entityManager.Remove(this);
			destroyed = true;
			if (Game.inputHandler.SelectedEntity == this)
			{
				Game.inputHandler.Deselect();	
			}
		}
		
		public abstract EntityType GetEntityType ();
		public abstract IEntityTemplate GetEntityTemplate();
		
		
		
		public bool IsOwnedBy(Team owner)
		{
			return Owner == owner;	
		}
		
		public bool IsOwnedByMainPlayer()
		{
			return Owner == Team.mainPlayer;	
		}
		
		public Tiles.Tile GetCurrentTile(World world)
		{
			return world.GetTile(PosX, PosY);	
		}
		
		public void StartTurn(World world)
		{
			foreach(var module in turnTriggerModules)
			{
				module.StartTurn();	
			}
		}

		#region IEntity implementation
		public int GetWorldX ()
		{
			return PosX;
		}
	
		public int GetWorldY ()
		{
			return PosY;
		}
		
		public Team GetOwner()
		{
			return Owner;	
		}

	
		public bool IsDestroyed ()
		{
			return destroyed;
		}

		
		public virtual void PlaceAt(World world, int worldX, int worldY)
		{
			world.entityManager.Remove(this);
			PosX = worldX;
			PosY = worldY;
			world.entityManager.Add(this);	
			world.NotifyMove(this);
		}


		public int GetManhattenDistanceTo (int x, int y)
		{
			return Math.Abs(x - GetWorldX()) + Math.Abs(y - GetWorldY());
		}

		public Conquest.Assets.Entity.StanceType GetStance ()
		{
			return Stance;
		}
		
		public void SetStance(StanceType stance)
		{
			Stance = stance;
		}
		#endregion
	}
}

