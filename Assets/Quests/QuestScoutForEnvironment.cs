using Conquest.Assets.Teams;
using Conquest.Assets.Quests.Rewards;
using Conquest.Assets.Environments;
using Conquest.Assets.Entity.Movement;

namespace Conquest.Assets.Quests
{
	public class QuestScoutForEnvironment: QuestBasic, IMoveListener
	{
		private Environment envir;
		
		public QuestScoutForEnvironment (World world, Team owner, IReward reward, Environment environment): base(world, owner, reward)
		{
			envir = environment;
		}
		
		public override void AcceptCallback(World world)
		{
			base.AcceptCallback(world);
			world.RegisterMoveListener(this);
		}

		#region implemented abstract members of Conquest.Assets.Quests.QuestBasic
		public override string GetQuestTitle ()
		{
			return "Scout out " + envir + " environment";
		}

		public override string GetQuestDescription ()
		{
			return "Locate and scout out any " + envir + " environment! Exploration is probably the key to success.";
		}

		protected override string GetQuestCompletionNotification ()
		{
			return "Hoorah! You have successfully located and explored the " + envir + "! You must be proud!";
		}
		#endregion

		#region IMoveListener implementation
		public void NotifyMove (Conquest.World world, Conquest.Assets.Entity.Modules.Interfaces.IEntity movedEntity)
		{
			int xPos = movedEntity.GetWorldX();
			int yPos = movedEntity.GetWorldY();
			
			if (Expanse.Eden.GetEnvironmentAt(xPos, yPos) == envir)
			{
				world.DeregisterMoveListener(this);
				CompleteQuest();
			}
		}
		#endregion
	}
}

