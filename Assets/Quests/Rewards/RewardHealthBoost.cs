using System;
using Conquest.Assets.Entity.Modules.Interfaces;
using System.Collections.Generic;

namespace Conquest.Assets.Quests.Rewards
{
	public class RewardHealthBoost: IReward
	{
		private int amount;
		
		public RewardHealthBoost (int amount)
		{
			this.amount = amount;
		}
		
		#region IReward implementation
		public void ClaimReward (Conquest.Assets.Teams.Team player)
		{
			List<IEntity> entities = player.GetEntities();
			
			foreach (IEntity e in entities)
			{
				IHealthy healthy = e as IHealthy;
				
				if (healthy != null)
				{
					healthy.GetHealthModule().IncreaseMaxHealth(amount);	
				}
			}
		}
	
		public string GetRewardDescription ()
		{
			return "All your units will receive a boost of " + amount + " points to their health on completion of this quest! How lovely!";
		}

	
		public string GetRewardClaimReport ()
		{
			
			return "Your grace, I am pleased to report that all your men feel stronger with an increase of " + amount + " health points!";
		}
		#endregion
	}
}

