using System;

namespace Conquest.Assets.Quests.Rewards
{
	public class RewardNothing: IReward
	{
		public RewardNothing ()
		{
		}

		#region IReward implementation
		public void ClaimReward (Conquest.Assets.Teams.Team player)
		{
			
		}
	
		public string GetRewardDescription ()
		{
			return "There will be no reward for your efforts, but why not do it anyway?";
		}

	
		public string GetRewardClaimReport ()
		{
			return "Sadly, you gain nothing for your efforts!";
		}
		#endregion
	}
}

