using System;
using Conquest.Assets.Teams;

namespace Conquest.Assets.Quests.Rewards
{
	public interface IReward
	{
		void ClaimReward(Team player);
		string GetRewardClaimReport();
		string GetRewardDescription();
	}
	
}

