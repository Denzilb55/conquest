using System;
using Conquest.Assets.Teams;
using UnityEngine;

namespace Conquest.Assets.Quests.Rewards
{
	public class RewardPlayerPointIncrease: IReward
	{
		private int rewardCount;
		
		public RewardPlayerPointIncrease (int amount)
		{
			rewardCount = amount;
			rewardString[0] = "Your strategy is permanently extended! You will now, hopefully, enjoy " + amount + " extra Player Points each turn!";
		}

		#region IReward implementation
		public void ClaimReward (Team player)
		{
			player.DefaultActivePoints += rewardCount;			
		}
	
		public string GetRewardDescription ()
		{
			return "This quest is worth " + rewardCount + " extra Player Points each turn! What a spectacular boost to your already wonderful strategic ability!";
		}
		
		public string GetRewardClaimReport ()
		{
			return rewardString[UnityEngine.Random.Range(0, rewardString.Length)];
		}
		#endregion
		
		private readonly string [] rewardString = new string[1];

	}
}

