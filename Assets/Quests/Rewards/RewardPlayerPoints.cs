using System;
using Conquest.Assets.Teams;
using UnityEngine;

namespace Conquest.Assets.Quests.Rewards
{
	public class RewardPlayerPoints: IReward
	{
		private int rewardCount;
		
		public RewardPlayerPoints (int amount)
		{
			rewardCount = amount;
			rewardString[0] = "You earn " + amount + " Player Points!";
			rewardString[1] = "Take these " + amount + " Player Points for your terribly hard efforts!";
		}

		#region IReward implementation
		public void ClaimReward (Team player)
		{
			player.RewardPlayerPoints(rewardCount);
		}
	
		public string GetRewardDescription ()
		{
			return "This quest is worth " + rewardCount + " Player Points! A good opportunity to extend your strategy!";
		}
		
		public string GetRewardClaimReport ()
		{
			return rewardString[UnityEngine.Random.Range(0, rewardString.Length)];
		}
		#endregion
		
		private readonly string [] rewardString = new string[2];
	}
	
}

