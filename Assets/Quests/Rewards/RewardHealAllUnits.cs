using System;
using System.Collections.Generic;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Quests.Rewards
{
	public class RewardHealAllUnits: IReward
	{
		private int amount;
		
		public RewardHealAllUnits (int amount)
		{
			this.amount = amount;
		}
		
		#region IReward implementation
		public void ClaimReward (Conquest.Assets.Teams.Team player)
		{
			List<IEntity> entities = player.GetEntities();
			
			foreach (IEntity e in entities)
			{
				IHealthy healthy = e as IHealthy;
				
				if (healthy != null)
				{
					healthy.GetHealthModule().Heal(amount);	
				}
			}
		}
	
		public string GetRewardDescription ()
		{
			return "All your units will be healed by " + amount + " points on completion of this quest! Think about the health and wellbeing of your men, my lord!";
		}

	
		public string GetRewardClaimReport ()
		{
			
			return "Your grace, I am pleased to report that all your men feel healthier by " + amount + " points!";
		}
		#endregion
	}
}

