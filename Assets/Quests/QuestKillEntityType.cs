using System;
using Conquest.Assets.Entity;
using Conquest.Assets.Teams;
using Conquest.Assets.Quests.Rewards;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest.Assets.Quests
{
	public class QuestKillEntityType: QuestBasic, IDestroyListener
	{
		public readonly int TargetAmount;
		public int KilledAmount { get; private set; }
		public EntityType entityType { get; private set; }

		
		public QuestKillEntityType (World world, int amount, EntityType entityType, Team owner, IReward reward): base(world, owner, reward)
		{
			TargetAmount = amount;
			this.entityType = entityType;
		}
		
		
		public override void AcceptCallback(World world)
		{
			base.AcceptCallback(world);
			world.RegisterDestroyListener(this);
		}

		#region IDestroyListener implementation
		public void NotifyDestroy (World world, IEntity entity, IEntity destroyer)
		{

			if (destroyer.GetOwner() == Owner && entity.GetEntityType() == entityType)
			{
				KilledAmount++;
				if (KilledAmount >= TargetAmount)
				{
					world.DeregisterDestroyListener(this);
					CompleteQuest();	
				}
			}
		}
		#endregion

		#region implemented abstract members of Conquest.Assets.Quests.QuestBasic
		public override string GetQuestTitle ()
		{
			String entityPart = entityType.ToString();
			
			if (TargetAmount > 1)
			{
				entityPart += "s";
			}
			
			return "Kill " + TargetAmount + " " + entityPart;
		}

		public override string GetQuestDescription ()
		{
			
			return "You have killed " + KilledAmount + " of " + TargetAmount + " " + entityType.ToString() + "s.";
		}

		protected override string GetQuestCompletionNotification ()
		{
			return entityType.ToString() + " killing quest completed!";
		}


		#endregion
	}
}

