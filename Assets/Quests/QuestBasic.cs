using System;
using Conquest.Assets.Teams;
using Conquest.Assets.Quests.Rewards;
using Conquest.Assets.Graphics.UtilityGUI;
using Conquest.Assets.Graphics;
using UnityEngine;

namespace Conquest.Assets.Quests
{
	public abstract class QuestBasic
	{
		public Team Owner { get; private set; }
		public bool Completed { get; private set; }
		public IReward Reward { get; private set; }
		
		public QuestBasic (World world, Team owner, IReward reward)
		{
			Owner = owner;
			Completed = false;
			Reward = reward;
		}
		
		public QuestBasic (World world, Team owner)
		{
			Owner = owner;
			Completed = false;
			Reward = DefinedRewards.Nothing;
		}
		
		protected void CompleteQuest()
		{
			if (Completed) return;
			
			Completed = true;	
			if (Owner == Team.mainPlayer)
			{
				NotifyPlayer();	
				Reward.ClaimReward(Owner);	
			}
			Owner.quests.Remove(this);
		}
		
		public virtual void AcceptCallback(World world)
		{
			
		}
		
		public void SetOwner(Team team)
		{
			Owner = team;	
		}
		
		private void NotifyPlayer()
		{
			string [] message = new string[3];
			int [] spacing = new int[3];
			GUIStyle [] styles = new GUIStyle[3];
			
			message[0] = GetQuestCompletionNotification();
			spacing[0] = 15;
			styles[0] = GUIManager.titleStyle;
			message[1] = GetQuestDescription();
			spacing[1] = 30;
			styles[1] = GUIManager.reportStyle;
			message[2] = GetQuestRewardClaimReport();
			spacing[2] = 30;
			styles[2] = GUIManager.reportStyle;
			Game.guiManager.SetSpecialMessage(message, Mood.ReportSuccess, spacing, styles);

		}
		
		
		public abstract string GetQuestTitle();
		public abstract string GetQuestDescription();
		protected abstract string GetQuestCompletionNotification();
		
		public string GetQuestRewardDescription()
		{
			return Reward.GetRewardDescription();	
		}
		
		protected string GetQuestRewardClaimReport()
		{
			return Reward.GetRewardClaimReport();	
		}

	}
}

