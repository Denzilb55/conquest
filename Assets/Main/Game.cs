using UnityEngine;

using System.Collections.Generic;
using Conquest;
using Conquest.Assets.GameInput;
using Conquest.Assets.Graphics;

[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class Game : MonoBehaviour 
{
	
	public Texture2D textureAtlas;
	public Texture2D guiBar;
		
	public SpriteSheet spriteSheet;
	private Mesh worldMesh;
	private GameGraphics gg;
	public static World world;
	private bool started = false;
	public static InputHandler inputHandler;
	public static GUIManager guiManager;
	
	// Use this for initialization
	void Start () {
		textureAtlas = (Texture2D)Resources.Load("Sprites");
		

		spriteSheet = new SpriteSheet(textureAtlas);
		Sprite.InitializeSprites(spriteSheet);
		
		worldMesh = new Mesh();
		MeshFilter filter = GetComponent<MeshFilter>();
        filter.mesh = worldMesh;
		
		MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material.SetTexture("_MainTex", Sprite.textureAtlas);
       // meshRenderer.material.shader = Shader.Find("Unlit/Transparent Cutout");
		//meshRenderer.material.shader = Shader.Find("Unlit/Texture");
		
		gg = new GameGraphics(worldMesh, 16000);
		world = new World();
		inputHandler = new InputHandler(world);
		started = true;
		guiManager = new GUIManager();
		GameGUI.guiManager = guiManager;
		world.Initialize();
	}
	
	// Update is called once per frame
	void Update () {
		if (!started) return;
		
		//Smoother frame rate by collecting garbage every frame
		System.GC.Collect();
        System.GC.WaitForPendingFinalizers();
		
		gg.Initialise(16000);
		world.Step();
		inputHandler.movementAnimator.Step(world);
		
		gg.BuildMesh(world, inputHandler);
		
		inputHandler.HandleInputs();
	}
	
	
}
