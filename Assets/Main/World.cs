using Conquest.Assets.Graphics;
using System.Collections.Generic;
using UnityEngine;
using Conquest.Assets.Entity;
using Conquest.Assets.Entity.Management;
using Conquest.Assets.Entity.Movement;
using Conquest.Assets.Tiles;
using Conquest.Assets.Environments;
using Conquest.Assets.UtilityGeneral;
using Conquest.Assets.Teams;
using Conquest.Assets.Entity.Stats;
using Conquest.Assets.Quests;
using Conquest.Assets.Quests.Rewards;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Main;

namespace Conquest
{
	public class World
	{
		public EntityManager entityManager = new EntityManager();
		public EntitySpawner entitySpawner = new EntitySpawner();
		public TurnHandler turnHandler = new TurnHandler();
		public int ticks;
		
		
		public const int WORLD_WIDTH = 160;
		public const int WORLD_HEIGHT = 160;
		public bool [,] explored = new bool[WORLD_WIDTH,WORLD_HEIGHT];
		public int [,] tiles = new int[WORLD_WIDTH,WORLD_HEIGHT];
		/*
		 * STORE TILES AS IDs TO TILES AND NOT TILE OBJECTS!! Give great performance benefit
		 * for large worlds. Saving many objects is bad for GC performance. Saving data as primitives
		 * cost no GC (Garbage Collector) penalty
		 */
		
		public float ScrollX {get; private set;}
		public float ScrollY {get; private set;}
		
		
		/*
		 * EVENT LISTENERS
		 */
		private readonly List<IDestroyListener> destroyListeners = new List<IDestroyListener>();
		private readonly List<IDestroyListener> destroyListenerRemovals = new List<IDestroyListener>();
		private readonly List<IMoveListener> moveListeners = new List<IMoveListener>();
		private readonly List<IMoveListener> moveListenerRemovals = new List<IMoveListener>();
		
		
		public int ScrollXi
		{
			get {return (int) ScrollX; }
		}
		
		public int ScrollYi
		{
			get {return (int) ScrollY; }
		}
		
		public int ScrollWorldX
		{
			get {return ScrollXi / SpriteSheet.ICON_WIDTH; }	
		}
		
		public int ScrollWorldY
		{
			get {return ScrollYi / SpriteSheet.ICON_HEIGHT; }	
		}
		
		public int ScrollAreaX
		{
			get {return ScrollWorldX / EntityManager.AREA_SIZE; }	
		}
		
		public int ScrollAreaY
		{
			get {return ScrollWorldY / EntityManager.AREA_SIZE; }	
		}
			
	
		public World ()
		{
			RegisterDestroyListener(Team.neutralPlayer);
			RegisterMoveListener(Team.neutralPlayer);
		}
		
		
		public void Initialize()
		{
			GenerateWorld();
			AssignInitialQuests();
		}
		
		public void AssignInitialQuests()
		{
			new QuestKillEntityType(this, 3, EntityType.Bandit, Team.mainPlayer, new RewardHealthBoost(10));
			new QuestKillEntityType(this, 5, EntityType.Soldier, Team.mainPlayer, new RewardPlayerPointIncrease(5));
			
			Environment env = Expanse.Eden.GetEnvironmentAt(UnityEngine.Random.Range(0, WORLD_WIDTH), UnityEngine.Random.Range(0, WORLD_HEIGHT));
			
			new QuestScoutForEnvironment(this, Team.mainPlayer, new RewardPlayerPoints(25), env);
		}
		
		
		public void GenerateWorld()
		{
			GenerateTerrain();
			SpawnUnits();
		}
		
		private void GenerateTerrain()
		{
			for (int i = 0; i < WORLD_WIDTH; i++)
			{
				for (int j = 0; j < WORLD_HEIGHT; j++)
				{
					tiles[i,j] = Expanse.Eden.GetTileIDAt(i,j);
					//tiles[i,j] = Environment.Continent.GetTileIDAt(i,j);
				}
			}
		}
		
		private void SpawnUnits()
		{
			PlaceMainPlayer();
			entitySpawner.SpawnInitial(this);
		}
		
		private void PlaceMainPlayer()
		{
			LinkedList<Environment> validEnvironments = new LinkedList<Environment>();
			
			validEnvironments.AddLast(Environment.Desert);
			validEnvironments.AddLast(Environment.GrassPlain);
			validEnvironments.AddLast(Environment.PlainField);
			validEnvironments.AddLast(Environment.Jungle);
			validEnvironments.AddLast(Environment.DustyField);
			validEnvironments.AddLast(Environment.DustPlains);
			validEnvironments.AddLast(Environment.IslandLake);
			
			bool donePlacing = false;
			
			int x = 0;
			int y = 0;
			int timeOut = 0;
			
			while (!donePlacing)
			{
				x = Random.Range(0, WORLD_WIDTH);
				y = Random.Range(0, WORLD_HEIGHT);
				
				Environment env = Expanse.Eden.GetEnvironmentAt(x, y);
				
				if (validEnvironments.Contains(env))
				{
					donePlacing = true;
					break;
				}
				
				if (timeOut++ > 2000)
				{
					GenerateWorld();
					return;
				}
			}
			
			LinkedList<Tile> tilesList = new LinkedList<Tile>(); //unallowed tiles
			tilesList.AddLast(Tile.Water);
			for (int i = 0; i < 16; i++)
			{
				EntityBandit unit = new EntityBandit(x,y, Team.mainPlayer, StanceType.Main);
				PlaceUnitNear(unit,x,y,tilesList,false);
				Team.neutralPlayer.AddPriorityTarget(unit);
			}
			
			tilesList.AddLast(Tile.Tree);
			for (int i = 0; i < 10; i++)
			{
				EntitySoldier unit = new EntitySoldier(x,y, Team.mainPlayer, StanceType.Main);
				PlaceUnitNear(unit,x,y,tilesList,false);
				Team.neutralPlayer.AddPriorityTarget(unit);
			}
			
			EntityBandit bh = new EntityBandit(0,0, Team.neutralPlayer, StanceType.NeutralAggressive);
			PlaceUnitNear(bh,x,y,null,false);
			
			FocusViewportOnTile(x, y);
		}
		
		public bool PlaceUnitNear(IEntity entity, int x, int y, LinkedList<Tile> tilesList, bool allowed)
		{
			int tolerance = 0;
			
			do
			{
				int placeX = x + Random.Range(-tolerance/10, tolerance/10+1);
				int placeY = y + Random.Range(-tolerance/10, tolerance/10+1);
				
				if (placeX < 0) placeX = 0;
				if (placeX >= WORLD_WIDTH) placeX = WORLD_WIDTH - 1;
				if (placeY < 0) placeY = 0;
				if (placeY >= WORLD_HEIGHT) placeY = WORLD_HEIGHT - 1;
				
				Tile tile = Tile.tiles[tiles[placeX, placeY]];
				
				bool tileFound = (tilesList != null) && (tilesList.Contains(tile));
				
				if (!(allowed ^ tileFound) && entityManager.GetEntityAt(placeX, placeY) == null)
				{
					entity.PlaceAt(this, placeX, placeY);
					return true;
				}
			}
			while (tolerance++ < 100);	
			return false;
		}
		

		
		
		public void Step()
		{
			ticks++;
			TeamAI teamAI = turnHandler.ActivePlayer as TeamAI;
			
			if (teamAI != null)
			{
				teamAI.Tick(this);	
			}
			
			entitySpawner.Tick(ticks, this);
			
			foreach (IDestroyListener destroyListener in destroyListenerRemovals)
			{
				destroyListeners.Remove(destroyListener);	
			}
			
			
			foreach (IMoveListener moveListerner in moveListenerRemovals)
			{
				moveListeners.Remove(moveListerner);	
			}
		}
		
		
		public Tile GetTile(int xWorld, int yWorld)
		{
			return Tile.tiles[tiles[xWorld,yWorld]];	
		}
		
		
		public void Render(GameGraphics g)
		{
			int xStart = (ScrollXi - Screen.width/2)/SpriteSheet.ICON_WIDTH;
			if (xStart < 0) xStart = 0;
			
			int yStart = (ScrollYi - Screen.height/2)/SpriteSheet.ICON_HEIGHT;
			if (yStart < 0) yStart = 0;
			
			int xEnd = (ScrollXi + Screen.width/2)/SpriteSheet.ICON_WIDTH +1;
			if (xEnd > WORLD_WIDTH) xEnd = WORLD_WIDTH;
			
			int yEnd = (ScrollYi + Screen.height/2)/SpriteSheet.ICON_HEIGHT +1;
			if (yEnd > WORLD_HEIGHT) yEnd = WORLD_HEIGHT;
			
			for (int i = xStart; i < xEnd; i++)
			{
				for (int j = yStart; j < yEnd; j++)
				{
					int spriteIndex = 0;
					
					if (explored[i,j])
					{
						Tile t = GetTile(i,j);
						spriteIndex = t.GetSprite.index;
					}
					else
					{
						spriteIndex = Sprite.Unexplored.index;
					}
					
					g.RenderAtlasTile(spriteIndex, i * SpriteSheet.ICON_WIDTH - ScrollXi, j * SpriteSheet.ICON_HEIGHT - ScrollYi, SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
				}
			}
			
			
		
			List<IEntity> entities = entityManager.GetEntitiesAroundArea(ScrollAreaX, ScrollAreaY);
			foreach (IEntity e in entities)
			{
				Rectangle viewport = new Rectangle(ScrollXi - Screen.width/2, ScrollYi - Screen.height/2, Screen.width, Screen.height);
				if (e.IsInViewport(viewport) && explored[e.GetWorldX(), e.GetWorldY()])
				{
					e.Render(g, ScrollXi, ScrollYi);	
				}
			}
			
		
			IEntity selectedEntity = Game.inputHandler.SelectedEntity;
			if (selectedEntity != null)
			{
			
				if (!Game.inputHandler.movementAnimator.IsAnimating())
				{
					g.RenderTileToWorldCoords(this, Sprite.SelectMarker.index, selectedEntity.GetWorldX(), selectedEntity.GetWorldY(), SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
				}
				
				IAttacker attacker = selectedEntity as IAttacker;
				
				if (attacker != null)
				{
					List<IEntity> attackableEntities = attacker.GetAttackModule().GetInRangeEntities(this);
					
					foreach (IEntity e in attackableEntities)
					{
						g.RenderTileToWorldCoords(this, Sprite.AttackMarker.index, e.GetWorldX(), e.GetWorldY(), SpriteSheet.ICON_WIDTH, SpriteSheet.ICON_HEIGHT);
					}
				}
			}
		}
		
		public void NotifyDestroy(IEntity entity, IEntity destroyer)
		{
			foreach (IDestroyListener listener in destroyListeners)
			{
				listener.NotifyDestroy(this, entity, destroyer);
			}
		}
		
		public void NotifyMove(IEntity movedEntity)
		{
			foreach (IMoveListener listener in moveListeners)
			{
				listener.NotifyMove(this, movedEntity);	
			}
			
			if (movedEntity.GetOwner() == Team.mainPlayer)
			{
				ExploreTerrain(movedEntity);
			}
		}
		
		private void ExploreTerrain(IEntity entity)
		{
		//	bool[,] exploreMatrix = new bool[8*2+1, 8*2+1];
			int x = entity.GetWorldX();
			int y = entity.GetWorldY();
			//ExploreTerrainSpread(x - 8, y - 8, x, y, 8, exploreMatrix);
			
			int xStart = x - 8;
			int yStart = y - 8;
			
			int xEnd = x + 8;
			int yEnd = y + 8;
			
			xStart = xStart < 0 ? 0 : xStart;
			yStart = yStart < 0 ? 0 : yStart;
			xEnd = xEnd >= WORLD_WIDTH ? WORLD_WIDTH - 1 : xEnd;
			yEnd = yEnd >= WORLD_HEIGHT ? WORLD_HEIGHT - 1 : yEnd;
			

			for (int i = xStart; i < xEnd; i++)
			{
				for (int j = yStart; j < yEnd; j++)
				{
					if (entity.GetManhattenDistanceTo(i, j) <= 14)
					{
						explored[i,j] = true;	
					}
				}
			}
		}
		
		private void ExploreTerrainSpread(int xOffset, int yOffset, int x, int y, int spread, bool[,] exploreMatrix)
		{
			if (x >= 0 && x < WORLD_WIDTH && y >= 0 && y < WORLD_HEIGHT)
			{
				//UnityEngine.Debug.Log(x +" " + y);
				if (exploreMatrix[x - xOffset,y - yOffset]) return;
				explored[x,y] = true;
				exploreMatrix[x - xOffset,y - yOffset] = true;
			}
			else
			{
				return;	
			}

			spread--;
			if (spread > 0)
			{
				ExploreTerrainSpread(xOffset, yOffset, x-1, y, spread, exploreMatrix);	
				ExploreTerrainSpread(xOffset, yOffset, x+1, y, spread, exploreMatrix);
				ExploreTerrainSpread(xOffset, yOffset, x, y+1, spread, exploreMatrix);
				ExploreTerrainSpread(xOffset, yOffset, x, y-1, spread, exploreMatrix);
			}
		}
		
		public void RegisterDestroyListener(IDestroyListener listener)
		{
			destroyListeners.Add(listener);	
		}
		
		public void DeregisterDestroyListener(IDestroyListener listener)
		{
			destroyListenerRemovals.Add(listener);	
		}
		
		public void RegisterMoveListener(IMoveListener listener)
		{
			moveListeners.Add(listener);
		}
		
		public void DeregisterMoveListener(IMoveListener listener)
		{
			moveListenerRemovals.Add(listener);
		}
		
		public void ScrollRight(float pixels)
		{
			ScrollX += pixels;
			LimitScrollX();
		}
		
		private void LimitScrollX()
		{
			if (ScrollX < Screen.width/2)
			{
				ScrollX = Screen.width/2;	
			}
			else if (ScrollX >= WORLD_WIDTH * SpriteSheet.ICON_WIDTH - Screen.width/2)
			{
				ScrollX = WORLD_WIDTH * SpriteSheet.ICON_WIDTH - Screen.width/2;	
			}
		}
		
		private void LimitScrollY()
		{
			int guiScroll = ((int)(GameGUI.GUI_HEIGHT / SpriteSheet.ICON_HEIGHT)) * SpriteSheet.ICON_HEIGHT;
			
			if (ScrollY < Screen.height/2)
			{
				ScrollY = Screen.height/2;	
			}
			else if (ScrollY >= WORLD_HEIGHT * SpriteSheet.ICON_HEIGHT - Screen.height/2 + guiScroll*1.2)
			{
				ScrollY = WORLD_HEIGHT * SpriteSheet.ICON_HEIGHT - Screen.height/2 + guiScroll*1.2f;	
			}
		}
		
		public void ScrollDown(float pixels)
		{
			ScrollY += pixels;
			LimitScrollY();
		}
		
		public void FocusViewport(float xPixels, float yPixels)
		{
			ScrollX = xPixels;
			ScrollY = yPixels;
			
			LimitScrollX();
			LimitScrollY();
		}
		
		public void FocusViewportOnTile(int xWorld, int yWorld)
		{
			ScrollX = xWorld << SpriteSheet.ICON_SHIFT_WIDTH;
			ScrollY = yWorld << SpriteSheet.ICON_SHIFT_HEIGHT;
			
			LimitScrollX();
			LimitScrollY();
		}
	}
}

