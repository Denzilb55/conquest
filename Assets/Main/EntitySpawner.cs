using Conquest.Assets.Entity;
using Conquest.Assets.Entity.Modules.Interfaces;
using Conquest.Assets.Teams;
using Conquest.Assets.Tiles;
using UnityEngine;
using System.Collections.Generic;

namespace Conquest.Assets.Main
{
	public class EntitySpawner
	{
		private readonly int maxSpawn;
		private const int TIMEOUT_MAX = 200;
		
		public EntitySpawner ()
		{
			maxSpawn = World.WORLD_HEIGHT * World.WORLD_WIDTH / (8 * 8);
		}
		
		public void SpawnInitial(World world)
		{
			int spawned = 0;
			int timeout = 0;
			
			while (spawned < maxSpawn || timeout == TIMEOUT_MAX)
			{
				IEntity unit = null;
				LinkedList<Tile> unallowedTiles = new LinkedList<Tile>();
				unallowedTiles.AddLast(Tile.Water);
				
				int rand = Random.Range(0, 14);
				
				if (rand < 3)
				{
					unit = new EntitySoldier(0,0,Team.neutralPlayer, StanceType.NeutralAggressive);
				}
				else if (rand < 13)
				{
					unit = new EntityBandit(0,0,Team.neutralPlayer, StanceType.NeutralAggressive);
				}
				else
				{
					unit = new EntityBountyHunter(0,0,Team.neutralPlayer, StanceType.NeutralPassive);
				}
				
				int x = Random.Range(0, World.WORLD_WIDTH);
				int y = Random.Range(0, World.WORLD_HEIGHT);
				
				
				
				if (world.PlaceUnitNear(unit, x, y, unallowedTiles, false))
				{
					spawned++;
				}
				else
				{
					timeout++;
				}
				
			}

		}
		
		public void Tick(int ticks, World world)
		{
			
		}
	}
}

