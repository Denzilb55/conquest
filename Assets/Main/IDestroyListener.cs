using System;
using Conquest.Assets.Entity;
using Conquest.Assets.Entity.Modules.Interfaces;

namespace Conquest
{
	public interface IDestroyListener
	{
		void NotifyDestroy(World world, IEntity entity, IEntity destroyer);
	}
}

