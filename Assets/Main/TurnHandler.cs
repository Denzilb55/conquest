using System;
using Conquest.Assets.Teams;
using Conquest.Assets.Graphics;

namespace Conquest
{
	public class TurnHandler
	{
		private Team activePlayer;
		
		public Team ActivePlayer
		{
			get { return activePlayer; }
		}
		
		private int ActiveID
		{
			get { return activePlayer.ID; }	
		}
		
		public TurnHandler ()
		{
			activePlayer = Team.mainPlayer;
		}
		
		public bool IsMainPlayerActive()
		{
			return ActivePlayer == Team.mainPlayer;	
		}
		
		public void CycleTurn(World world)
		{
			int currentActive = ActiveID;
			currentActive++;
			
			if (currentActive >= Team.TEAM_COUNT)
			{
				currentActive = 0;	
			}
			
			activePlayer = Team.teams[currentActive];
			Game.guiManager.StartExpireState(1.5f, GUIManager.GUIState.ReportActiveTurn); 

			StartTurn(activePlayer, world);
		}
		
		private void StartTurn(Team player, World world)
		{
			activePlayer.StartTurn(world);
		}

	}
}

